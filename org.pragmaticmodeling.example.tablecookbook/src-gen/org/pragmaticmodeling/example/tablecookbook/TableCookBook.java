package org.pragmaticmodeling.example.tablecookbook;

import com.google.common.collect.Iterables;
import fr.pragmaticmodeling.pxdoc.Bold;
import fr.pragmaticmodeling.pxdoc.BorderType;
import fr.pragmaticmodeling.pxdoc.Cell;
import fr.pragmaticmodeling.pxdoc.CellDef;
import fr.pragmaticmodeling.pxdoc.Color;
import fr.pragmaticmodeling.pxdoc.ContainerElement;
import fr.pragmaticmodeling.pxdoc.Document;
import fr.pragmaticmodeling.pxdoc.HeadingN;
import fr.pragmaticmodeling.pxdoc.Italic;
import fr.pragmaticmodeling.pxdoc.Measure;
import fr.pragmaticmodeling.pxdoc.MergeKind;
import fr.pragmaticmodeling.pxdoc.Row;
import fr.pragmaticmodeling.pxdoc.ShadingPattern;
import fr.pragmaticmodeling.pxdoc.StyleApplication;
import fr.pragmaticmodeling.pxdoc.Table;
import fr.pragmaticmodeling.pxdoc.Underline;
import fr.pragmaticmodeling.pxdoc.UnderlineType;
import fr.pragmaticmodeling.pxdoc.UnitKind;
import fr.pragmaticmodeling.pxdoc.VerticalAlignmentType;
import fr.pragmaticmodeling.pxdoc.runtime.AbstractPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.PxDocGenerationException;
import java.io.File;
import java.util.concurrent.ThreadLocalRandom;
import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;

@SuppressWarnings("all")
public class TableCookBook extends AbstractPxDocGenerator {
  private Logger logger = Logger.getLogger(getClass());
  
  public TableCookBook() {
    super();
  }
  
  public String getName() {
    return "TableCookBook";
  }
  
  public void generate() throws PxDocGenerationException {
    org.eclipse.emf.ecore.EPackage model = (org.eclipse.emf.ecore.EPackage)launcher.getModel().getInstance();
    try {
    	logger.info("----------------------------------------------------------------");
    	logger.info("Starting TableCookBook generation...");
    	checkStylesheet(getStylesheetName());
    	main(null, model);
    } catch (Exception e) {
    	throw new PxDocGenerationException(e);
    } catch (NoClassDefFoundError e) {
        logger.error(e.getMessage());
    } finally {
    	logger.info("FINISHED: TableCookBook generation...");
    }
  }
  
  @Override
  public String getStylesheetName() {
    return "TableCBStylesheet";
  }
  
  public String randomColor() {
    int r = ThreadLocalRandom.current().nextInt(0, 255);
    int g = ThreadLocalRandom.current().nextInt(0, 255);
    int b = ThreadLocalRandom.current().nextInt(0, 255);
    String _plus = (Integer.valueOf(r) + ",");
    String _plus_1 = (_plus + Integer.valueOf(g));
    String _plus_2 = (_plus_1 + ",");
    return (_plus_2 + Integer.valueOf(b));
  }
  
  public void error(final String message) {
    logger.error(message);
  }
  
  public void info(final String message) {
    logger.info(message);
  }
  
  /**
   * Entry point.
   */
  public void main(final ContainerElement parent, final EPackage model) throws PxDocGenerationException {
    String _file = (new File(getLauncher().getTargetFile())).getAbsolutePath();
    Document lObj0 = createDocument(parent, _file, "TableCBStylesheet", null, createMeasure(getHeaderHeight(), UnitKind.CM), createMeasure(getFooterHeight(), UnitKind.CM));
    addDocument(lObj0);
    {
      HeadingN lObj1 = createHeadingN(lObj0, 1, false);
      createText(lObj1, "Tables tests");
      HeadingN lObj2 = createHeadingN(lObj0, 2, false);
      createText(lObj2, "No widths specified");
      StyleApplication lObj3 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj3, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table {\r\n    row header shadingColor:\"192,192,192\" shadingPattern:pct20 {\r\n        cell {\"EClass\" }\r\n        cell {\"Abstract\" }\r\n    }\r\n    for (eClass : model.getEClassifiers.filter(EClass)) {\r\n        row {\r\n            cell {eClass.name }\r\n            cell {eClass.abstract }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj4 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj4, "Result");
      Table lObj5 = createTable(lObj0, null, null, null, null, ShadingPattern.NO_VALUE);
      {
        Row lObj6 = createRow(lObj5, null, true, "192,192,192", ShadingPattern.PCT20, false, null);
        {
          Cell lObj7 = createCell(lObj6);
          createText(lObj7, "EClass");
          Cell lObj8 = createCell(lObj6);
          createText(lObj8, "Abstract");
        }
        Iterable<EClass> _filter = Iterables.<EClass>filter(model.getEClassifiers(), EClass.class);
        for (final EClass eClass : _filter) {
          Row lObj9 = createRow(lObj5, null, false, null, ShadingPattern.NO_VALUE, false, null);
          {
            Cell lObj10 = createCell(lObj9);
            createText(lObj10, eClass.getName());
            Cell lObj11 = createCell(lObj9);
            createText(lObj11, String.valueOf(eClass.isAbstract()));
          }
        }
      }
      HeadingN lObj12 = createHeadingN(lObj0, 2, false);
      createText(lObj12, "Left constraint, No widths specified");
      StyleApplication lObj13 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj13, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table left:5cm {\r\n    row header shadingColor:\"192,192,192\" shadingPattern:pct20 {\r\n        cell {\"EClass\" }\r\n        cell {\"Abstract\" }\r\n    }\r\n    for (eClass : model.getEClassifiers.filter(EClass)) {\r\n        row {\r\n            cell {eClass.name }\r\n            cell {eClass.abstract }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj14 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj14, "Result");
      Measure tableLeft15 = createMeasure((float)5, UnitKind.CM);
      Table lObj16 = createTable(lObj0, tableLeft15, null, null, null, ShadingPattern.NO_VALUE);
      {
        Row lObj17 = createRow(lObj16, null, true, "192,192,192", ShadingPattern.PCT20, false, null);
        {
          Cell lObj18 = createCell(lObj17);
          createText(lObj18, "EClass");
          Cell lObj19 = createCell(lObj17);
          createText(lObj19, "Abstract");
        }
        Iterable<EClass> _filter = Iterables.<EClass>filter(model.getEClassifiers(), EClass.class);
        for (final EClass eClass : _filter) {
          Row lObj20 = createRow(lObj16, null, false, null, ShadingPattern.NO_VALUE, false, null);
          {
            Cell lObj21 = createCell(lObj20);
            createText(lObj21, eClass.getName());
            Cell lObj22 = createCell(lObj20);
            createText(lObj22, String.valueOf(eClass.isAbstract()));
          }
        }
      }
      HeadingN lObj23 = createHeadingN(lObj0, 2, false);
      createText(lObj23, "Row heights, no widths specified");
      StyleApplication lObj24 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj24, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table {\r\n    row header height:1cm shadingColor:\"192,192,192\" shadingPattern:pct20 {\r\n        cell {\"EClass (row header height:1cm shadingColor:\\\"192,192,192\\\" shadingPattern:pct20) {...})\" }\r\n        cell {\"Abstract\" }\r\n    }\r\n    for (eClass : model.getEClassifiers.filter(EClass)) {\r\n        row height:500twips {\r\n            cell {eClass.name \" (row height: 500 twips {...})\" }\r\n            cell {eClass.abstract }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj25 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj25, "Result");
      Table lObj26 = createTable(lObj0, null, null, null, null, ShadingPattern.NO_VALUE);
      {
        Measure rowHeight27 = createMeasure((float)1, UnitKind.CM);
        Row lObj28 = createRow(lObj26, rowHeight27, true, "192,192,192", ShadingPattern.PCT20, false, null);
        {
          Cell lObj29 = createCell(lObj28);
          createText(lObj29, "EClass (row header height:1cm shadingColor:\"192,192,192\" shadingPattern:pct20) {...})");
          Cell lObj30 = createCell(lObj28);
          createText(lObj30, "Abstract");
        }
        Iterable<EClass> _filter = Iterables.<EClass>filter(model.getEClassifiers(), EClass.class);
        for (final EClass eClass : _filter) {
          Measure rowHeight31 = createMeasure((float)500, UnitKind.TWIPS);
          Row lObj32 = createRow(lObj26, rowHeight31, false, null, ShadingPattern.NO_VALUE, false, null);
          {
            Cell lObj33 = createCell(lObj32);
            {
              createText(lObj33, eClass.getName());
              createText(lObj33, " (row height: 500 twips {...})");
            }
            Cell lObj34 = createCell(lObj32);
            createText(lObj34, String.valueOf(eClass.isAbstract()));
          }
        }
      }
      HeadingN lObj35 = createHeadingN(lObj0, 2, false);
      createText(lObj35, "Table width, without any cols widths");
      StyleApplication lObj36 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj36, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table width:50pc {\r\n    row header shadingColor:\"192,192,192\" shadingPattern:pct20 {\r\n        cell {\"EClass\" }\r\n        cell {\"Abstract\" }\r\n    }\r\n    for (eClass : model.getEClassifiers.filter(EClass)) {\r\n        row {\r\n            cell {eClass.name }\r\n            cell {eClass.abstract }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj37 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj37, "Result");
      Measure tableWidth38 = createMeasure((float)50, UnitKind.PC);
      Table lObj39 = createTable(lObj0, null, tableWidth38, null, null, ShadingPattern.NO_VALUE);
      {
        Row lObj40 = createRow(lObj39, null, true, "192,192,192", ShadingPattern.PCT20, false, null);
        {
          Cell lObj41 = createCell(lObj40);
          createText(lObj41, "EClass");
          Cell lObj42 = createCell(lObj40);
          createText(lObj42, "Abstract");
        }
        Iterable<EClass> _filter = Iterables.<EClass>filter(model.getEClassifiers(), EClass.class);
        for (final EClass eClass : _filter) {
          Row lObj43 = createRow(lObj39, null, false, null, ShadingPattern.NO_VALUE, false, null);
          {
            Cell lObj44 = createCell(lObj43);
            createText(lObj44, eClass.getName());
            Cell lObj45 = createCell(lObj43);
            createText(lObj45, String.valueOf(eClass.isAbstract()));
          }
        }
      }
      HeadingN lObj46 = createHeadingN(lObj0, 2, false);
      createText(lObj46, "A cell overrides a color defined for the entire row");
      StyleApplication lObj47 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj47, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table {\r\n    row header shadingColor:\"0,255,0\" {\r\n        cell {\"EClass\" }\r\n        cell [ shadingColor:\"128,0,255\" shadingPattern:solid ] {\"override row specified color\" }\r\n    }\r\n    for (eClass : model.getEClassifiers.filter(EClass)) {\r\n        row {\r\n            cell {eClass.name }\r\n            cell {eClass.abstract }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj48 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj48, "Result");
      Table lObj49 = createTable(lObj0, null, null, null, null, ShadingPattern.NO_VALUE);
      {
        Row lObj50 = createRow(lObj49, null, true, "0,255,0", ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj51 = createCell(lObj50);
          createText(lObj51, "EClass");
          Cell lObj52 = createCell(lObj50);
          createCellDef(lObj52, null, null, "128,0,255", ShadingPattern.SOLID, VerticalAlignmentType.TOP);
          createText(lObj52, "override row specified color");
        }
        Iterable<EClass> _filter = Iterables.<EClass>filter(model.getEClassifiers(), EClass.class);
        for (final EClass eClass : _filter) {
          Row lObj53 = createRow(lObj49, null, false, null, ShadingPattern.NO_VALUE, false, null);
          {
            Cell lObj54 = createCell(lObj53);
            createText(lObj54, eClass.getName());
            Cell lObj55 = createCell(lObj53);
            createText(lObj55, String.valueOf(eClass.isAbstract()));
          }
        }
      }
      HeadingN lObj56 = createHeadingN(lObj0, 2, false);
      createText(lObj56, "Col widths specified on the table");
      StyleApplication lObj57 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj57, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table [ width:30pc | width:70pc ] {\r\n    row header shadingColor:\"192,192,192\" shadingPattern:pct20 {\r\n        cell {\"EClass\" }\r\n        cell {\"Abstract\" }\r\n    }\r\n    for (eClass : model.getEClassifiers.filter(EClass)) {\r\n        row {\r\n            cell {eClass.name }\r\n            cell {eClass.abstract }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj58 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj58, "Result");
      Table lObj59 = createTable(lObj0, null, null, null, null, ShadingPattern.NO_VALUE);
      Measure cellWidth60 = createMeasure((float)30, UnitKind.PC);
      createCellDef(lObj59, null, cellWidth60, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      Measure cellWidth61 = createMeasure((float)70, UnitKind.PC);
      createCellDef(lObj59, null, cellWidth61, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      {
        Row lObj62 = createRow(lObj59, null, true, "192,192,192", ShadingPattern.PCT20, false, null);
        {
          Cell lObj63 = createCell(lObj62);
          createText(lObj63, "EClass");
          Cell lObj64 = createCell(lObj62);
          createText(lObj64, "Abstract");
        }
        Iterable<EClass> _filter = Iterables.<EClass>filter(model.getEClassifiers(), EClass.class);
        for (final EClass eClass : _filter) {
          Row lObj65 = createRow(lObj59, null, false, null, ShadingPattern.NO_VALUE, false, null);
          {
            Cell lObj66 = createCell(lObj65);
            createText(lObj66, eClass.getName());
            Cell lObj67 = createCell(lObj65);
            createText(lObj67, String.valueOf(eClass.isAbstract()));
          }
        }
      }
      HeadingN lObj68 = createHeadingN(lObj0, 2, false);
      createText(lObj68, "Col widths specified on the row header");
      StyleApplication lObj69 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj69, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table {\r\n    row header shadingColor:\"192,192,192\" shadingPattern:pct20 [ width:30pc | width:70pc ] {\r\n        cell {\"EClass\" }\r\n        cell {\"Abstract\" }\r\n    }\r\n    for (eClass : model.getEClassifiers.filter(EClass)) {\r\n        row {\r\n            cell {eClass.name }\r\n            cell {eClass.abstract }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj70 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj70, "Result");
      Table lObj71 = createTable(lObj0, null, null, null, null, ShadingPattern.NO_VALUE);
      {
        Row lObj72 = createRow(lObj71, null, true, "192,192,192", ShadingPattern.PCT20, false, null);
        Measure cellWidth73 = createMeasure((float)30, UnitKind.PC);
        createCellDef(lObj72, null, cellWidth73, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
        Measure cellWidth74 = createMeasure((float)70, UnitKind.PC);
        createCellDef(lObj72, null, cellWidth74, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
        {
          Cell lObj75 = createCell(lObj72);
          createText(lObj75, "EClass");
          Cell lObj76 = createCell(lObj72);
          createText(lObj76, "Abstract");
        }
        Iterable<EClass> _filter = Iterables.<EClass>filter(model.getEClassifiers(), EClass.class);
        for (final EClass eClass : _filter) {
          Row lObj77 = createRow(lObj71, null, false, null, ShadingPattern.NO_VALUE, false, null);
          {
            Cell lObj78 = createCell(lObj77);
            createText(lObj78, eClass.getName());
            Cell lObj79 = createCell(lObj77);
            createText(lObj79, String.valueOf(eClass.isAbstract()));
          }
        }
      }
      HeadingN lObj80 = createHeadingN(lObj0, 2, false);
      createText(lObj80, "Col widths specified on data rows");
      StyleApplication lObj81 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj81, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table {\r\n    row header shadingColor:\"192,192,192\" shadingPattern:pct20  {\r\n        cell {\"EClass\" }\r\n        cell {\"Abstract\" }\r\n    }\r\n    for (eClass : model.getEClassifiers.filter(EClass)) {\r\n        row [ width:30pc | width:70pc ] {\r\n            cell {eClass.name }\r\n            cell {eClass.abstract }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj82 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj82, "Result");
      Table lObj83 = createTable(lObj0, null, null, null, null, ShadingPattern.NO_VALUE);
      {
        Row lObj84 = createRow(lObj83, null, true, "192,192,192", ShadingPattern.PCT20, false, null);
        {
          Cell lObj85 = createCell(lObj84);
          createText(lObj85, "EClass");
          Cell lObj86 = createCell(lObj84);
          createText(lObj86, "Abstract");
        }
        Iterable<EClass> _filter = Iterables.<EClass>filter(model.getEClassifiers(), EClass.class);
        for (final EClass eClass : _filter) {
          Row lObj87 = createRow(lObj83, null, false, null, ShadingPattern.NO_VALUE, false, null);
          Measure cellWidth88 = createMeasure((float)30, UnitKind.PC);
          createCellDef(lObj87, null, cellWidth88, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          Measure cellWidth89 = createMeasure((float)70, UnitKind.PC);
          createCellDef(lObj87, null, cellWidth89, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          {
            Cell lObj90 = createCell(lObj87);
            createText(lObj90, eClass.getName());
            Cell lObj91 = createCell(lObj87);
            createText(lObj91, String.valueOf(eClass.isAbstract()));
          }
        }
      }
      HeadingN lObj92 = createHeadingN(lObj0, 2, false);
      createText(lObj92, "The table defines shadingColors");
      StyleApplication lObj93 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj93, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table [ shadingColor:\"255,0,128\" | shadingColor:\"128,128,255\" ] {\r\n    row header shadingColor:\"192,192,192\" shadingPattern:pct20  {\r\n        cell {\"EClass\" }\r\n        cell {\"Abstract\" }\r\n    }\r\n    for (eClass : model.getEClassifiers.filter(EClass)) {\r\n        row [ width:30pc | width:70pc ] {\r\n            cell {eClass.name }\r\n            cell {eClass.abstract }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj94 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj94, "Result");
      Table lObj95 = createTable(lObj0, null, null, null, null, ShadingPattern.NO_VALUE);
      createCellDef(lObj95, null, null, "255,0,128", ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      createCellDef(lObj95, null, null, "128,128,255", ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      {
        Row lObj96 = createRow(lObj95, null, true, "192,192,192", ShadingPattern.PCT20, false, null);
        {
          Cell lObj97 = createCell(lObj96);
          createText(lObj97, "EClass");
          Cell lObj98 = createCell(lObj96);
          createText(lObj98, "Abstract");
        }
        Iterable<EClass> _filter = Iterables.<EClass>filter(model.getEClassifiers(), EClass.class);
        for (final EClass eClass : _filter) {
          Row lObj99 = createRow(lObj95, null, false, null, ShadingPattern.NO_VALUE, false, null);
          Measure cellWidth100 = createMeasure((float)30, UnitKind.PC);
          createCellDef(lObj99, null, cellWidth100, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          Measure cellWidth101 = createMeasure((float)70, UnitKind.PC);
          createCellDef(lObj99, null, cellWidth101, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          {
            Cell lObj102 = createCell(lObj99);
            createText(lObj102, eClass.getName());
            Cell lObj103 = createCell(lObj99);
            createText(lObj103, String.valueOf(eClass.isAbstract()));
          }
        }
      }
      HeadingN lObj104 = createHeadingN(lObj0, 2, false);
      createText(lObj104, "Col widths specified on each cell");
      StyleApplication lObj105 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj105, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table {\r\n    row header shadingColor:\"192,192,192\" shadingPattern:pct20  {\r\n        cell {\"EClass\" }\r\n        cell {\"Abstract\" }\r\n    }\r\n    for (eClass : model.getEClassifiers.filter(EClass)) {\r\n        row {\r\n            cell [ width:30pc ] {eClass.name }\r\n            cell [ width:70pc ] {eClass.abstract }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj106 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj106, "Result");
      Table lObj107 = createTable(lObj0, null, null, null, null, ShadingPattern.NO_VALUE);
      {
        Row lObj108 = createRow(lObj107, null, true, "192,192,192", ShadingPattern.PCT20, false, null);
        {
          Cell lObj109 = createCell(lObj108);
          createText(lObj109, "EClass");
          Cell lObj110 = createCell(lObj108);
          createText(lObj110, "Abstract");
        }
        Iterable<EClass> _filter = Iterables.<EClass>filter(model.getEClassifiers(), EClass.class);
        for (final EClass eClass : _filter) {
          Row lObj111 = createRow(lObj107, null, false, null, ShadingPattern.NO_VALUE, false, null);
          {
            Cell lObj112 = createCell(lObj111);
            Measure cellWidth113 = createMeasure((float)30, UnitKind.PC);
            createCellDef(lObj112, null, cellWidth113, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
            createText(lObj112, eClass.getName());
            Cell lObj114 = createCell(lObj111);
            Measure cellWidth115 = createMeasure((float)70, UnitKind.PC);
            createCellDef(lObj114, null, cellWidth115, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
            createText(lObj114, String.valueOf(eClass.isAbstract()));
          }
        }
      }
      HeadingN lObj116 = createHeadingN(lObj0, 2, false);
      createText(lObj116, "Col widths specified cells and rows");
      StyleApplication lObj117 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj117, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table {\r\n    row header shadingColor:\"192,192,192\" shadingPattern:pct20  {\r\n        cell {\"EClass\" }\r\n        cell {\"Abstract\" }\r\n    }\r\n    for (eClass : model.getEClassifiers.filter(EClass)) {\r\n        row [ width:70pc | width:30pc ] {\r\n            cell [ width:30pc ] {eClass.name }\r\n            cell [ width:70pc ] {eClass.abstract }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj118 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj118, "Result");
      Table lObj119 = createTable(lObj0, null, null, null, null, ShadingPattern.NO_VALUE);
      {
        Row lObj120 = createRow(lObj119, null, true, "192,192,192", ShadingPattern.PCT20, false, null);
        {
          Cell lObj121 = createCell(lObj120);
          createText(lObj121, "EClass");
          Cell lObj122 = createCell(lObj120);
          createText(lObj122, "Abstract");
        }
        Iterable<EClass> _filter = Iterables.<EClass>filter(model.getEClassifiers(), EClass.class);
        for (final EClass eClass : _filter) {
          Row lObj123 = createRow(lObj119, null, false, null, ShadingPattern.NO_VALUE, false, null);
          Measure cellWidth124 = createMeasure((float)70, UnitKind.PC);
          createCellDef(lObj123, null, cellWidth124, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          Measure cellWidth125 = createMeasure((float)30, UnitKind.PC);
          createCellDef(lObj123, null, cellWidth125, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          {
            Cell lObj126 = createCell(lObj123);
            Measure cellWidth127 = createMeasure((float)30, UnitKind.PC);
            createCellDef(lObj126, null, cellWidth127, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
            createText(lObj126, eClass.getName());
            Cell lObj128 = createCell(lObj123);
            Measure cellWidth129 = createMeasure((float)70, UnitKind.PC);
            createCellDef(lObj128, null, cellWidth129, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
            createText(lObj128, String.valueOf(eClass.isAbstract()));
          }
        }
      }
      HeadingN lObj130 = createHeadingN(lObj0, 2, false);
      createText(lObj130, "Col widths specified cells, rows, and table");
      StyleApplication lObj131 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj131, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table [ width:80pc | width:20pc ] {\r\n    row header shadingColor:\"192,192,192\" shadingPattern:pct20  {\r\n        cell {\"EClass\" }\r\n        cell {\"Abstract\" }\r\n    }\r\n    for (eClass : model.getEClassifiers.filter(EClass)) {\r\n        row [ width:70pc | width:30pc ] {\r\n            cell [ width:30pc ] {eClass.name }\r\n            cell [ width:70pc ] {eClass.abstract }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj132 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj132, "Result");
      Table lObj133 = createTable(lObj0, null, null, null, null, ShadingPattern.NO_VALUE);
      Measure cellWidth134 = createMeasure((float)80, UnitKind.PC);
      createCellDef(lObj133, null, cellWidth134, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      Measure cellWidth135 = createMeasure((float)20, UnitKind.PC);
      createCellDef(lObj133, null, cellWidth135, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      {
        Row lObj136 = createRow(lObj133, null, true, "192,192,192", ShadingPattern.PCT20, false, null);
        {
          Cell lObj137 = createCell(lObj136);
          createText(lObj137, "EClass");
          Cell lObj138 = createCell(lObj136);
          createText(lObj138, "Abstract");
        }
        Iterable<EClass> _filter = Iterables.<EClass>filter(model.getEClassifiers(), EClass.class);
        for (final EClass eClass : _filter) {
          Row lObj139 = createRow(lObj133, null, false, null, ShadingPattern.NO_VALUE, false, null);
          Measure cellWidth140 = createMeasure((float)70, UnitKind.PC);
          createCellDef(lObj139, null, cellWidth140, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          Measure cellWidth141 = createMeasure((float)30, UnitKind.PC);
          createCellDef(lObj139, null, cellWidth141, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          {
            Cell lObj142 = createCell(lObj139);
            Measure cellWidth143 = createMeasure((float)30, UnitKind.PC);
            createCellDef(lObj142, null, cellWidth143, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
            createText(lObj142, eClass.getName());
            Cell lObj144 = createCell(lObj139);
            Measure cellWidth145 = createMeasure((float)70, UnitKind.PC);
            createCellDef(lObj144, null, cellWidth145, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
            createText(lObj144, String.valueOf(eClass.isAbstract()));
          }
        }
      }
      HeadingN lObj146 = createHeadingN(lObj0, 2, false);
      createText(lObj146, "Col widths changing everywhere");
      StyleApplication lObj147 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj147, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table [ width:70pc | width:30pc ] {\r\n    row header shadingColor:\"192,192,192\" shadingPattern:pct20  {\r\n        cell {\"EClass\" }\r\n        cell {\"Abstract\" }\r\n    }\r\n    for (eClass : model.getEClassifiers.filter(EClass)) {\r\n        var width1 = ThreadLocalRandom.current().nextInt(10, 90)\r\n        var width2 = 100 - width1\r\n        row [ width:width1 pc | width:width2 pc ] {\r\n            cell {eClass.name \" (width = \" width1 \"%)\" }\r\n            cell {eClass.abstract \" (width = \" width2 \"%)\" }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj148 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj148, "Result");
      Table lObj149 = createTable(lObj0, null, null, null, null, ShadingPattern.NO_VALUE);
      Measure cellWidth150 = createMeasure((float)70, UnitKind.PC);
      createCellDef(lObj149, null, cellWidth150, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      Measure cellWidth151 = createMeasure((float)30, UnitKind.PC);
      createCellDef(lObj149, null, cellWidth151, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      {
        Row lObj152 = createRow(lObj149, null, true, "192,192,192", ShadingPattern.PCT20, false, null);
        {
          Cell lObj153 = createCell(lObj152);
          createText(lObj153, "EClass");
          Cell lObj154 = createCell(lObj152);
          createText(lObj154, "Abstract");
        }
        Iterable<EClass> _filter = Iterables.<EClass>filter(model.getEClassifiers(), EClass.class);
        for (final EClass eClass : _filter) {
          {
            int width1 = ThreadLocalRandom.current().nextInt(10, 90);
            int width2 = (100 - width1);
            Row lObj155 = createRow(lObj149, null, false, null, ShadingPattern.NO_VALUE, false, null);
            Measure cellWidth156 = createMeasure((float)width1, UnitKind.PC);
            createCellDef(lObj155, null, cellWidth156, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
            Measure cellWidth157 = createMeasure((float)width2, UnitKind.PC);
            createCellDef(lObj155, null, cellWidth157, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
            {
              Cell lObj158 = createCell(lObj155);
              {
                createText(lObj158, eClass.getName());
                createText(lObj158, " (width = ");
                createText(lObj158, String.valueOf(width1));
                createText(lObj158, "%)");
              }
              Cell lObj159 = createCell(lObj155);
              {
                createText(lObj159, String.valueOf(eClass.isAbstract()));
                createText(lObj159, " (width = ");
                createText(lObj159, String.valueOf(width2));
                createText(lObj159, "%)");
              }
            }
          }
        }
      }
      HeadingN lObj160 = createHeadingN(lObj0, 2, false);
      createText(lObj160, "Table with random color for cells");
      StyleApplication lObj161 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj161, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table [ width:70pc | width:30pc ] {\r\n    row header shadingColor:\"192,192,192\" shadingPattern:pct20  {\r\n        cell {\"EClass\" }\r\n        cell {\"Abstract\" }\r\n    }\r\n    for (eClass : model.getEClassifiers.filter(EClass)) {\r\n        row {\r\n            var String color1 = randomColor\r\n            var String color2 = randomColor\r\n            cell [ shadingColor: color1 shadingPattern:pct50] {\r\n                eClass.name\r\n                \" (color = \"\r\n                color1\r\n                \")\"\r\n            }\r\n            cell [ shadingColor: color2 shadingPattern:pct50] {eClass.abstract \" (color = \" color2 \")\" }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj162 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj162, "Result");
      Table lObj163 = createTable(lObj0, null, null, null, null, ShadingPattern.NO_VALUE);
      Measure cellWidth164 = createMeasure((float)70, UnitKind.PC);
      createCellDef(lObj163, null, cellWidth164, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      Measure cellWidth165 = createMeasure((float)30, UnitKind.PC);
      createCellDef(lObj163, null, cellWidth165, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      {
        Row lObj166 = createRow(lObj163, null, true, "192,192,192", ShadingPattern.PCT20, false, null);
        {
          Cell lObj167 = createCell(lObj166);
          createText(lObj167, "EClass");
          Cell lObj168 = createCell(lObj166);
          createText(lObj168, "Abstract");
        }
        Iterable<EClass> _filter = Iterables.<EClass>filter(model.getEClassifiers(), EClass.class);
        for (final EClass eClass : _filter) {
          Row lObj169 = createRow(lObj163, null, false, null, ShadingPattern.NO_VALUE, false, null);
          {
            String color1 = this.randomColor();
            String color2 = this.randomColor();
            Cell lObj170 = createCell(lObj169);
            createCellDef(lObj170, null, null, color1, ShadingPattern.PCT50, VerticalAlignmentType.TOP);
            {
              createText(lObj170, eClass.getName());
              createText(lObj170, " (color = ");
              createText(lObj170, color1);
              createText(lObj170, ")");
            }
            Cell lObj171 = createCell(lObj169);
            createCellDef(lObj171, null, null, color2, ShadingPattern.PCT50, VerticalAlignmentType.TOP);
            {
              createText(lObj171, String.valueOf(eClass.isAbstract()));
              createText(lObj171, " (color = ");
              createText(lObj171, color2);
              createText(lObj171, ")");
            }
          }
        }
      }
      HeadingN lObj172 = createHeadingN(lObj0, 2, false);
      createText(lObj172, "Table borders");
      StyleApplication lObj173 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj173, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "for (borderType : BorderType.values) {\r\n    HeadingN level:2 + 1 {\"Table with \" borderType.name() \" border\" }\r\n    table [ borders(type:borderType) width:70pc | width:30pc ] {\r\n        row header shadingColor:\"192,192,192\" shadingPattern:pct20  {\r\n            cell {\"EClass\" }\r\n            cell {\"Abstract\" }\r\n        }\r\n        for (eClass : model.getEClassifiers.filter(EClass)) {\r\n            row {\r\n                cell {eClass.name }\r\n                cell {eClass.abstract }\r\n            }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj174 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj174, "Result");
      BorderType[] _values = BorderType.values();
      for (final BorderType borderType : _values) {
        {
          HeadingN lObj175 = createHeadingN(lObj0, (2 + 1), false);
          {
            createText(lObj175, "Table with ");
            createText(lObj175, borderType.name());
            createText(lObj175, " border");
          }
          Table lObj176 = createTable(lObj0, null, null, null, null, ShadingPattern.NO_VALUE);
          Measure cellWidth177 = createMeasure((float)70, UnitKind.PC);
          CellDef cellDef = createCellDef(lObj176, null, cellWidth177, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          cellDef.setLeftBorder(createBorder(borderType, null, -1));
          cellDef.setRightBorder(createBorder(borderType, null, -1));
          cellDef.setTopBorder(createBorder(borderType, null, -1));
          cellDef.setBottomBorder(createBorder(borderType, null, -1));
          Measure cellWidth178 = createMeasure((float)30, UnitKind.PC);
          createCellDef(lObj176, null, cellWidth178, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          {
            Row lObj179 = createRow(lObj176, null, true, "192,192,192", ShadingPattern.PCT20, false, null);
            {
              Cell lObj180 = createCell(lObj179);
              createText(lObj180, "EClass");
              Cell lObj181 = createCell(lObj179);
              createText(lObj181, "Abstract");
            }
            Iterable<EClass> _filter = Iterables.<EClass>filter(model.getEClassifiers(), EClass.class);
            for (final EClass eClass : _filter) {
              Row lObj182 = createRow(lObj176, null, false, null, ShadingPattern.NO_VALUE, false, null);
              {
                Cell lObj183 = createCell(lObj182);
                createText(lObj183, eClass.getName());
                Cell lObj184 = createCell(lObj182);
                createText(lObj184, String.valueOf(eClass.isAbstract()));
              }
            }
          }
        }
      }
      HeadingN lObj185 = createHeadingN(lObj0, 2, false);
      createText(lObj185, "Table with random color for cells, border type and border at all");
      StyleApplication lObj186 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj186, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table [ width:70pc | width:30pc ] {\r\n    row header shadingColor:\"192,192,192\" shadingPattern:pct20  {\r\n        cell {\"EClass\" }\r\n        cell {\"Abstract\" }\r\n    }\r\n    for (eClass : model.getEClassifiers.filter(EClass)) {\r\n        row {\r\n            var String color1 = randomColor\r\n            var String color2 = randomColor\r\n\r\n            cell [ shadingColor: color1 shadingPattern:pct50] {\r\n                eClass.name\r\n                \" (color = \"\r\n                color1\r\n                \")\"\r\n            }\r\n            cell [ shadingColor: color2 shadingPattern:pct50] {eClass.abstract \" (color = \" color2 \")\" }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj187 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj187, "Result");
      Table lObj188 = createTable(lObj0, null, null, null, null, ShadingPattern.NO_VALUE);
      Measure cellWidth189 = createMeasure((float)70, UnitKind.PC);
      createCellDef(lObj188, null, cellWidth189, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      Measure cellWidth190 = createMeasure((float)30, UnitKind.PC);
      createCellDef(lObj188, null, cellWidth190, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      {
        Row lObj191 = createRow(lObj188, null, true, "192,192,192", ShadingPattern.PCT20, false, null);
        {
          Cell lObj192 = createCell(lObj191);
          createText(lObj192, "EClass");
          Cell lObj193 = createCell(lObj191);
          createText(lObj193, "Abstract");
        }
        Iterable<EClass> _filter = Iterables.<EClass>filter(model.getEClassifiers(), EClass.class);
        for (final EClass eClass : _filter) {
          Row lObj194 = createRow(lObj188, null, false, null, ShadingPattern.NO_VALUE, false, null);
          {
            String color1 = this.randomColor();
            String color2 = this.randomColor();
            Cell lObj195 = createCell(lObj194);
            createCellDef(lObj195, null, null, color1, ShadingPattern.PCT50, VerticalAlignmentType.TOP);
            {
              createText(lObj195, eClass.getName());
              createText(lObj195, " (color = ");
              createText(lObj195, color1);
              createText(lObj195, ")");
            }
            Cell lObj196 = createCell(lObj194);
            createCellDef(lObj196, null, null, color2, ShadingPattern.PCT50, VerticalAlignmentType.TOP);
            {
              createText(lObj196, String.valueOf(eClass.isAbstract()));
              createText(lObj196, " (color = ");
              createText(lObj196, color2);
              createText(lObj196, ")");
            }
          }
        }
      }
      HeadingN lObj197 = createHeadingN(lObj0, 2, false);
      createText(lObj197, "Table with merges defined on rows");
      StyleApplication lObj198 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj198, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table [ width:30pc | width:70pc ] left:2cm {\r\n    row header shadingColor:\"0,0,0\" shadingPattern:pct10 [ width:30pc | width:70pc ] {\r\n        cell {\"EClass\"}\r\n        cell {\"Abstract\"}\r\n    }\r\n    for (eClass : model.getEClassifiers.filter(EClass)) {\r\n        row [ width:30pc merge:start | width:70pc ] {\r\n            cell {\r\n                color \"0,128,0\" {\r\n                    bold {\r\n                        underline {\r\n                            italic {\r\n                                eClass.name\r\n                            }\r\n                        }\r\n                    }\r\n                }\r\n            }\r\n            cell {eClass.abstract}\r\n        }\r\n        if (!eClass.getEStructuralFeatures.isEmpty) {\r\n            row shadingColor:\"0,0,0\" shadingPattern:pct20 [ width:30pc merge:proceed| width:70pc ] {\r\n                cell {}\r\n                cell {\"Features\" }\r\n            }\r\n            row shadingColor:\"0,0,0\" shadingPattern:pct30 [ width:30pc merge:proceed| width:35pc | width:35pc ] {\r\n                cell {}\r\n                cell {\"Name\" }\r\n                cell {\"Type\" }\r\n            }\r\n            for (EStructuralFeature feature : eClass.getEStructuralFeatures) {\r\n                row [ width:30pc merge:proceed| width:35pc | width:35pc ] {\r\n                    cell {}\r\n                    cell {feature.name }\r\n                    cell {feature.getEType.name }\r\n                }\r\n            }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj199 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj199, "Result");
      Measure tableLeft200 = createMeasure((float)2, UnitKind.CM);
      Table lObj201 = createTable(lObj0, tableLeft200, null, null, null, ShadingPattern.NO_VALUE);
      Measure cellWidth202 = createMeasure((float)30, UnitKind.PC);
      createCellDef(lObj201, null, cellWidth202, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      Measure cellWidth203 = createMeasure((float)70, UnitKind.PC);
      createCellDef(lObj201, null, cellWidth203, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      {
        Row lObj204 = createRow(lObj201, null, true, "0,0,0", ShadingPattern.PCT10, false, null);
        Measure cellWidth205 = createMeasure((float)30, UnitKind.PC);
        createCellDef(lObj204, null, cellWidth205, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
        Measure cellWidth206 = createMeasure((float)70, UnitKind.PC);
        createCellDef(lObj204, null, cellWidth206, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
        {
          Cell lObj207 = createCell(lObj204);
          createText(lObj207, "EClass");
          Cell lObj208 = createCell(lObj204);
          createText(lObj208, "Abstract");
        }
        Iterable<EClass> _filter = Iterables.<EClass>filter(model.getEClassifiers(), EClass.class);
        for (final EClass eClass : _filter) {
          {
            Row lObj209 = createRow(lObj201, null, false, null, ShadingPattern.NO_VALUE, false, null);
            Measure cellWidth210 = createMeasure((float)30, UnitKind.PC);
            createCellDef(lObj209, MergeKind.START, cellWidth210, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
            Measure cellWidth211 = createMeasure((float)70, UnitKind.PC);
            createCellDef(lObj209, null, cellWidth211, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
            {
              Cell lObj212 = createCell(lObj209);
              Color lObj213 = createColor(lObj212, "0,128,0");
              Bold lObj214 = createBold(lObj213);
              Underline lObj215 = createUnderline(lObj214, UnderlineType.SINGLE);
              Italic lObj216 = createItalic(lObj215);
              createText(lObj216, eClass.getName());
              Cell lObj217 = createCell(lObj209);
              createText(lObj217, String.valueOf(eClass.isAbstract()));
            }
            boolean _isEmpty = eClass.getEStructuralFeatures().isEmpty();
            boolean _not = (!_isEmpty);
            if (_not) {
              Row lObj218 = createRow(lObj201, null, false, "0,0,0", ShadingPattern.PCT20, false, null);
              Measure cellWidth219 = createMeasure((float)30, UnitKind.PC);
              createCellDef(lObj218, MergeKind.PROCEED, cellWidth219, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
              Measure cellWidth220 = createMeasure((float)70, UnitKind.PC);
              createCellDef(lObj218, null, cellWidth220, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
              {
                Cell lObj221 = createCell(lObj218);
                Cell lObj222 = createCell(lObj218);
                createText(lObj222, "Features");
              }
              Row lObj223 = createRow(lObj201, null, false, "0,0,0", ShadingPattern.PCT30, false, null);
              Measure cellWidth224 = createMeasure((float)30, UnitKind.PC);
              createCellDef(lObj223, MergeKind.PROCEED, cellWidth224, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
              Measure cellWidth225 = createMeasure((float)35, UnitKind.PC);
              createCellDef(lObj223, null, cellWidth225, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
              Measure cellWidth226 = createMeasure((float)35, UnitKind.PC);
              createCellDef(lObj223, null, cellWidth226, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
              {
                Cell lObj227 = createCell(lObj223);
                Cell lObj228 = createCell(lObj223);
                createText(lObj228, "Name");
                Cell lObj229 = createCell(lObj223);
                createText(lObj229, "Type");
              }
              EList<EStructuralFeature> _eStructuralFeatures = eClass.getEStructuralFeatures();
              for (final EStructuralFeature feature : _eStructuralFeatures) {
                Row lObj230 = createRow(lObj201, null, false, null, ShadingPattern.NO_VALUE, false, null);
                Measure cellWidth231 = createMeasure((float)30, UnitKind.PC);
                createCellDef(lObj230, MergeKind.PROCEED, cellWidth231, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
                Measure cellWidth232 = createMeasure((float)35, UnitKind.PC);
                createCellDef(lObj230, null, cellWidth232, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
                Measure cellWidth233 = createMeasure((float)35, UnitKind.PC);
                createCellDef(lObj230, null, cellWidth233, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
                {
                  Cell lObj234 = createCell(lObj230);
                  Cell lObj235 = createCell(lObj230);
                  createText(lObj235, feature.getName());
                  Cell lObj236 = createCell(lObj230);
                  createText(lObj236, feature.getEType().getName());
                }
              }
            }
          }
        }
      }
      HeadingN lObj237 = createHeadingN(lObj0, 2, false);
      createText(lObj237, "Table with merges defined on cells");
      StyleApplication lObj238 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj238, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table [ width:30pc | width:70pc ] left:2cm {\r\n    row header shadingColor:\"0,0,0\" shadingPattern:pct10 [ width:30pc | width:70pc ] {\r\n        cell {\"EClass\"}\r\n        cell {\"Abstract\"}\r\n    }\r\n    for (eClass : model.getEClassifiers.filter(EClass)) {\r\n        row {\r\n            cell [ width:30pc merge:start] {\r\n                color \"0,128,0\" {\r\n                    bold {\r\n                        underline {\r\n                            italic {\r\n                                eClass.name\r\n                            }\r\n                        }\r\n                    }\r\n                }\r\n            }\r\n            cell [ width:70pc ] { eClass.abstract}\r\n        }\r\n        if (!eClass.getEStructuralFeatures.isEmpty) {\r\n            row shadingColor:\"0,0,0\" shadingPattern:pct20 {\r\n                cell [ width:30pc merge:proceed] {}\r\n                cell [ width:70pc ] {\"Features\" }\r\n            }\r\n            row shadingColor:\"0,0,0\" shadingPattern:pct30  {\r\n                cell [ width:30pc merge:proceed] {}\r\n                cell [ width:35pc ] {\"Name\" }\r\n                cell [ width:35pc ] {\"Type\" }\r\n            }\r\n            for (EStructuralFeature feature : eClass.getEStructuralFeatures) {\r\n                row {\r\n                    cell [ width:30pc merge:proceed] {}\r\n                    cell [ width:35pc ] {feature.name }\r\n                    cell [ width:35pc ] {feature.getEType.name }\r\n                }\r\n            }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj239 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj239, "Result");
      Measure tableLeft240 = createMeasure((float)2, UnitKind.CM);
      Table lObj241 = createTable(lObj0, tableLeft240, null, null, null, ShadingPattern.NO_VALUE);
      Measure cellWidth242 = createMeasure((float)30, UnitKind.PC);
      createCellDef(lObj241, null, cellWidth242, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      Measure cellWidth243 = createMeasure((float)70, UnitKind.PC);
      createCellDef(lObj241, null, cellWidth243, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      {
        Row lObj244 = createRow(lObj241, null, true, "0,0,0", ShadingPattern.PCT10, false, null);
        Measure cellWidth245 = createMeasure((float)30, UnitKind.PC);
        createCellDef(lObj244, null, cellWidth245, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
        Measure cellWidth246 = createMeasure((float)70, UnitKind.PC);
        createCellDef(lObj244, null, cellWidth246, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
        {
          Cell lObj247 = createCell(lObj244);
          createText(lObj247, "EClass");
          Cell lObj248 = createCell(lObj244);
          createText(lObj248, "Abstract");
        }
        Iterable<EClass> _filter = Iterables.<EClass>filter(model.getEClassifiers(), EClass.class);
        for (final EClass eClass : _filter) {
          {
            Row lObj249 = createRow(lObj241, null, false, null, ShadingPattern.NO_VALUE, false, null);
            {
              Cell lObj250 = createCell(lObj249);
              Measure cellWidth251 = createMeasure((float)30, UnitKind.PC);
              createCellDef(lObj250, MergeKind.START, cellWidth251, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
              Color lObj252 = createColor(lObj250, "0,128,0");
              Bold lObj253 = createBold(lObj252);
              Underline lObj254 = createUnderline(lObj253, UnderlineType.SINGLE);
              Italic lObj255 = createItalic(lObj254);
              createText(lObj255, eClass.getName());
              Cell lObj256 = createCell(lObj249);
              Measure cellWidth257 = createMeasure((float)70, UnitKind.PC);
              createCellDef(lObj256, null, cellWidth257, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
              createText(lObj256, String.valueOf(eClass.isAbstract()));
            }
            boolean _isEmpty = eClass.getEStructuralFeatures().isEmpty();
            boolean _not = (!_isEmpty);
            if (_not) {
              Row lObj258 = createRow(lObj241, null, false, "0,0,0", ShadingPattern.PCT20, false, null);
              {
                Cell lObj259 = createCell(lObj258);
                Measure cellWidth260 = createMeasure((float)30, UnitKind.PC);
                createCellDef(lObj259, MergeKind.PROCEED, cellWidth260, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
                Cell lObj261 = createCell(lObj258);
                Measure cellWidth262 = createMeasure((float)70, UnitKind.PC);
                createCellDef(lObj261, null, cellWidth262, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
                createText(lObj261, "Features");
              }
              Row lObj263 = createRow(lObj241, null, false, "0,0,0", ShadingPattern.PCT30, false, null);
              {
                Cell lObj264 = createCell(lObj263);
                Measure cellWidth265 = createMeasure((float)30, UnitKind.PC);
                createCellDef(lObj264, MergeKind.PROCEED, cellWidth265, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
                Cell lObj266 = createCell(lObj263);
                Measure cellWidth267 = createMeasure((float)35, UnitKind.PC);
                createCellDef(lObj266, null, cellWidth267, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
                createText(lObj266, "Name");
                Cell lObj268 = createCell(lObj263);
                Measure cellWidth269 = createMeasure((float)35, UnitKind.PC);
                createCellDef(lObj268, null, cellWidth269, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
                createText(lObj268, "Type");
              }
              EList<EStructuralFeature> _eStructuralFeatures = eClass.getEStructuralFeatures();
              for (final EStructuralFeature feature : _eStructuralFeatures) {
                Row lObj270 = createRow(lObj241, null, false, null, ShadingPattern.NO_VALUE, false, null);
                {
                  Cell lObj271 = createCell(lObj270);
                  Measure cellWidth272 = createMeasure((float)30, UnitKind.PC);
                  createCellDef(lObj271, MergeKind.PROCEED, cellWidth272, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
                  Cell lObj273 = createCell(lObj270);
                  Measure cellWidth274 = createMeasure((float)35, UnitKind.PC);
                  createCellDef(lObj273, null, cellWidth274, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
                  createText(lObj273, feature.getName());
                  Cell lObj275 = createCell(lObj270);
                  Measure cellWidth276 = createMeasure((float)35, UnitKind.PC);
                  createCellDef(lObj275, null, cellWidth276, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
                  createText(lObj275, feature.getEType().getName());
                }
              }
            }
          }
        }
      }
      HeadingN lObj277 = createHeadingN(lObj0, 2, false);
      createText(lObj277, "All Fill Patterns");
      Table lObj278 = createTable(lObj0, null, null, null, null, ShadingPattern.NO_VALUE);
      {
        String color1 = "0,0,0";
        String color2 = "255,0,0";
        Row lObj279 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj280 = createCell(lObj279);
          createCellDef(lObj280, null, null, color1, ShadingPattern.CLEAR, VerticalAlignmentType.TOP);
          createText(lObj280, "clear");
          Cell lObj281 = createCell(lObj279);
          createCellDef(lObj281, null, null, color2, ShadingPattern.CLEAR, VerticalAlignmentType.TOP);
          createText(lObj281, "clear");
        }
        Row lObj282 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj283 = createCell(lObj282);
          createCellDef(lObj283, null, null, color1, ShadingPattern.DIAG_CROSS, VerticalAlignmentType.TOP);
          createText(lObj283, "diagCross");
          Cell lObj284 = createCell(lObj282);
          createCellDef(lObj284, null, null, color2, ShadingPattern.DIAG_CROSS, VerticalAlignmentType.TOP);
          createText(lObj284, "diagCross");
        }
        Row lObj285 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj286 = createCell(lObj285);
          createCellDef(lObj286, null, null, color1, ShadingPattern.DIAG_STRIPE, VerticalAlignmentType.TOP);
          createText(lObj286, "diagStripe");
          Cell lObj287 = createCell(lObj285);
          createCellDef(lObj287, null, null, color2, ShadingPattern.DIAG_STRIPE, VerticalAlignmentType.TOP);
          createText(lObj287, "diagStripe");
        }
        Row lObj288 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj289 = createCell(lObj288);
          createCellDef(lObj289, null, null, color1, ShadingPattern.HORZ_CROSS, VerticalAlignmentType.TOP);
          createText(lObj289, "horzCross");
          Cell lObj290 = createCell(lObj288);
          createCellDef(lObj290, null, null, color2, ShadingPattern.HORZ_CROSS, VerticalAlignmentType.TOP);
          createText(lObj290, "horzCross");
        }
        Row lObj291 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj292 = createCell(lObj291);
          createCellDef(lObj292, null, null, color1, ShadingPattern.HORZ_STRIPE, VerticalAlignmentType.TOP);
          createText(lObj292, "horzStripe");
          Cell lObj293 = createCell(lObj291);
          createCellDef(lObj293, null, null, color2, ShadingPattern.HORZ_STRIPE, VerticalAlignmentType.TOP);
          createText(lObj293, "horzStripe");
        }
        Row lObj294 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj295 = createCell(lObj294);
          createCellDef(lObj295, null, null, color1, ShadingPattern.NIL, VerticalAlignmentType.TOP);
          createText(lObj295, "nil");
          Cell lObj296 = createCell(lObj294);
          createCellDef(lObj296, null, null, color2, ShadingPattern.NIL, VerticalAlignmentType.TOP);
          createText(lObj296, "nil");
        }
        Row lObj297 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj298 = createCell(lObj297);
          createCellDef(lObj298, null, null, color1, ShadingPattern.PCT5, VerticalAlignmentType.TOP);
          createText(lObj298, "pct5");
          Cell lObj299 = createCell(lObj297);
          createCellDef(lObj299, null, null, color2, ShadingPattern.PCT5, VerticalAlignmentType.TOP);
          createText(lObj299, "pct5");
        }
        Row lObj300 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj301 = createCell(lObj300);
          createCellDef(lObj301, null, null, color1, ShadingPattern.PCT10, VerticalAlignmentType.TOP);
          createText(lObj301, "pct10");
          Cell lObj302 = createCell(lObj300);
          createCellDef(lObj302, null, null, color2, ShadingPattern.PCT10, VerticalAlignmentType.TOP);
          createText(lObj302, "pct10");
        }
        Row lObj303 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj304 = createCell(lObj303);
          createCellDef(lObj304, null, null, color1, ShadingPattern.PCT12, VerticalAlignmentType.TOP);
          createText(lObj304, "pct12");
          Cell lObj305 = createCell(lObj303);
          createCellDef(lObj305, null, null, color2, ShadingPattern.PCT12, VerticalAlignmentType.TOP);
          createText(lObj305, "pct12");
        }
        Row lObj306 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj307 = createCell(lObj306);
          createCellDef(lObj307, null, null, color1, ShadingPattern.PCT15, VerticalAlignmentType.TOP);
          createText(lObj307, "pct15");
          Cell lObj308 = createCell(lObj306);
          createCellDef(lObj308, null, null, color2, ShadingPattern.PCT15, VerticalAlignmentType.TOP);
          createText(lObj308, "pct15");
        }
        Row lObj309 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj310 = createCell(lObj309);
          createCellDef(lObj310, null, null, color1, ShadingPattern.PCT20, VerticalAlignmentType.TOP);
          createText(lObj310, "pct20");
          Cell lObj311 = createCell(lObj309);
          createCellDef(lObj311, null, null, color2, ShadingPattern.PCT20, VerticalAlignmentType.TOP);
          createText(lObj311, "pct20");
        }
        Row lObj312 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj313 = createCell(lObj312);
          createCellDef(lObj313, null, null, color1, ShadingPattern.PCT25, VerticalAlignmentType.TOP);
          createText(lObj313, "pct25");
          Cell lObj314 = createCell(lObj312);
          createCellDef(lObj314, null, null, color2, ShadingPattern.PCT25, VerticalAlignmentType.TOP);
          createText(lObj314, "pct25");
        }
        Row lObj315 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj316 = createCell(lObj315);
          createCellDef(lObj316, null, null, color1, ShadingPattern.PCT30, VerticalAlignmentType.TOP);
          createText(lObj316, "pct30");
          Cell lObj317 = createCell(lObj315);
          createCellDef(lObj317, null, null, color2, ShadingPattern.PCT30, VerticalAlignmentType.TOP);
          createText(lObj317, "pct30");
        }
        Row lObj318 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj319 = createCell(lObj318);
          createCellDef(lObj319, null, null, color1, ShadingPattern.PCT35, VerticalAlignmentType.TOP);
          createText(lObj319, "pct35");
          Cell lObj320 = createCell(lObj318);
          createCellDef(lObj320, null, null, color2, ShadingPattern.PCT35, VerticalAlignmentType.TOP);
          createText(lObj320, "pct35");
        }
        Row lObj321 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj322 = createCell(lObj321);
          createCellDef(lObj322, null, null, color1, ShadingPattern.PCT37, VerticalAlignmentType.TOP);
          createText(lObj322, "pct37");
          Cell lObj323 = createCell(lObj321);
          createCellDef(lObj323, null, null, color2, ShadingPattern.PCT37, VerticalAlignmentType.TOP);
          createText(lObj323, "pct37");
        }
        Row lObj324 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj325 = createCell(lObj324);
          createCellDef(lObj325, null, null, color1, ShadingPattern.PCT40, VerticalAlignmentType.TOP);
          createText(lObj325, "pct40");
          Cell lObj326 = createCell(lObj324);
          createCellDef(lObj326, null, null, color2, ShadingPattern.PCT40, VerticalAlignmentType.TOP);
          createText(lObj326, "pct40");
        }
        Row lObj327 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj328 = createCell(lObj327);
          createCellDef(lObj328, null, null, color1, ShadingPattern.PCT45, VerticalAlignmentType.TOP);
          createText(lObj328, "pct45");
          Cell lObj329 = createCell(lObj327);
          createCellDef(lObj329, null, null, color2, ShadingPattern.PCT45, VerticalAlignmentType.TOP);
          createText(lObj329, "pct45");
        }
        Row lObj330 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj331 = createCell(lObj330);
          createCellDef(lObj331, null, null, color1, ShadingPattern.PCT50, VerticalAlignmentType.TOP);
          createText(lObj331, "pct50");
          Cell lObj332 = createCell(lObj330);
          createCellDef(lObj332, null, null, color2, ShadingPattern.PCT50, VerticalAlignmentType.TOP);
          createText(lObj332, "pct50");
        }
        Row lObj333 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj334 = createCell(lObj333);
          createCellDef(lObj334, null, null, color1, ShadingPattern.PCT55, VerticalAlignmentType.TOP);
          createText(lObj334, "pct55");
          Cell lObj335 = createCell(lObj333);
          createCellDef(lObj335, null, null, color2, ShadingPattern.PCT55, VerticalAlignmentType.TOP);
          createText(lObj335, "pct55");
        }
        Row lObj336 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj337 = createCell(lObj336);
          createCellDef(lObj337, null, null, color1, ShadingPattern.PCT60, VerticalAlignmentType.TOP);
          createText(lObj337, "pct60");
          Cell lObj338 = createCell(lObj336);
          createCellDef(lObj338, null, null, color2, ShadingPattern.PCT60, VerticalAlignmentType.TOP);
          createText(lObj338, "pct60");
        }
        Row lObj339 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj340 = createCell(lObj339);
          createCellDef(lObj340, null, null, color1, ShadingPattern.PCT62, VerticalAlignmentType.TOP);
          createText(lObj340, "pct62");
          Cell lObj341 = createCell(lObj339);
          createCellDef(lObj341, null, null, color2, ShadingPattern.PCT62, VerticalAlignmentType.TOP);
          createText(lObj341, "pct62");
        }
        Row lObj342 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj343 = createCell(lObj342);
          createCellDef(lObj343, null, null, color1, ShadingPattern.PCT65, VerticalAlignmentType.TOP);
          createText(lObj343, "pct65");
          Cell lObj344 = createCell(lObj342);
          createCellDef(lObj344, null, null, color2, ShadingPattern.PCT65, VerticalAlignmentType.TOP);
          createText(lObj344, "pct65");
        }
        Row lObj345 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj346 = createCell(lObj345);
          createCellDef(lObj346, null, null, color1, ShadingPattern.PCT70, VerticalAlignmentType.TOP);
          createText(lObj346, "pct70");
          Cell lObj347 = createCell(lObj345);
          createCellDef(lObj347, null, null, color2, ShadingPattern.PCT70, VerticalAlignmentType.TOP);
          createText(lObj347, "pct70");
        }
        Row lObj348 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj349 = createCell(lObj348);
          createCellDef(lObj349, null, null, color1, ShadingPattern.PCT75, VerticalAlignmentType.TOP);
          createText(lObj349, "pct75");
          Cell lObj350 = createCell(lObj348);
          createCellDef(lObj350, null, null, color2, ShadingPattern.PCT75, VerticalAlignmentType.TOP);
          createText(lObj350, "pct75");
        }
        Row lObj351 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj352 = createCell(lObj351);
          createCellDef(lObj352, null, null, color1, ShadingPattern.PCT80, VerticalAlignmentType.TOP);
          createText(lObj352, "pct80");
          Cell lObj353 = createCell(lObj351);
          createCellDef(lObj353, null, null, color2, ShadingPattern.PCT80, VerticalAlignmentType.TOP);
          createText(lObj353, "pct80");
        }
        Row lObj354 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj355 = createCell(lObj354);
          createCellDef(lObj355, null, null, color1, ShadingPattern.PCT85, VerticalAlignmentType.TOP);
          createText(lObj355, "pct85");
          Cell lObj356 = createCell(lObj354);
          createCellDef(lObj356, null, null, color2, ShadingPattern.PCT85, VerticalAlignmentType.TOP);
          createText(lObj356, "pct85");
        }
        Row lObj357 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj358 = createCell(lObj357);
          createCellDef(lObj358, null, null, color1, ShadingPattern.PCT87, VerticalAlignmentType.TOP);
          createText(lObj358, "pct87");
          Cell lObj359 = createCell(lObj357);
          createCellDef(lObj359, null, null, color2, ShadingPattern.PCT87, VerticalAlignmentType.TOP);
          createText(lObj359, "pct87");
        }
        Row lObj360 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj361 = createCell(lObj360);
          createCellDef(lObj361, null, null, color1, ShadingPattern.PCT90, VerticalAlignmentType.TOP);
          createText(lObj361, "pct90");
          Cell lObj362 = createCell(lObj360);
          createCellDef(lObj362, null, null, color2, ShadingPattern.PCT90, VerticalAlignmentType.TOP);
          createText(lObj362, "pct90");
        }
        Row lObj363 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj364 = createCell(lObj363);
          createCellDef(lObj364, null, null, color1, ShadingPattern.PCT95, VerticalAlignmentType.TOP);
          createText(lObj364, "pct95");
          Cell lObj365 = createCell(lObj363);
          createCellDef(lObj365, null, null, color2, ShadingPattern.PCT95, VerticalAlignmentType.TOP);
          createText(lObj365, "pct95");
        }
        Row lObj366 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj367 = createCell(lObj366);
          createCellDef(lObj367, null, null, color1, ShadingPattern.REVERSE_DIAG_STRIPE, VerticalAlignmentType.TOP);
          createText(lObj367, "reverseDiagStripe");
          Cell lObj368 = createCell(lObj366);
          createCellDef(lObj368, null, null, color2, ShadingPattern.REVERSE_DIAG_STRIPE, VerticalAlignmentType.TOP);
          createText(lObj368, "reverseDiagStripe");
        }
        Row lObj369 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj370 = createCell(lObj369);
          createCellDef(lObj370, null, null, color1, ShadingPattern.SOLID, VerticalAlignmentType.TOP);
          createText(lObj370, "solid");
          Cell lObj371 = createCell(lObj369);
          createCellDef(lObj371, null, null, color2, ShadingPattern.SOLID, VerticalAlignmentType.TOP);
          createText(lObj371, "solid");
        }
        Row lObj372 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj373 = createCell(lObj372);
          createCellDef(lObj373, null, null, color1, ShadingPattern.THIN_DIAG_CROSS, VerticalAlignmentType.TOP);
          createText(lObj373, "thinDiagCross");
          Cell lObj374 = createCell(lObj372);
          createCellDef(lObj374, null, null, color2, ShadingPattern.THIN_DIAG_CROSS, VerticalAlignmentType.TOP);
          createText(lObj374, "thinDiagCross");
        }
        Row lObj375 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj376 = createCell(lObj375);
          createCellDef(lObj376, null, null, color1, ShadingPattern.THIN_DIAG_STRIPE, VerticalAlignmentType.TOP);
          createText(lObj376, "thinDiagStripe");
          Cell lObj377 = createCell(lObj375);
          createCellDef(lObj377, null, null, color2, ShadingPattern.THIN_DIAG_STRIPE, VerticalAlignmentType.TOP);
          createText(lObj377, "thinDiagStripe");
        }
        Row lObj378 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj379 = createCell(lObj378);
          createCellDef(lObj379, null, null, color1, ShadingPattern.THIN_HORZ_CROSS, VerticalAlignmentType.TOP);
          createText(lObj379, "thinHorzCross");
          Cell lObj380 = createCell(lObj378);
          createCellDef(lObj380, null, null, color2, ShadingPattern.THIN_HORZ_CROSS, VerticalAlignmentType.TOP);
          createText(lObj380, "thinHorzCross");
        }
        Row lObj381 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj382 = createCell(lObj381);
          createCellDef(lObj382, null, null, color1, ShadingPattern.THIN_HORZ_STRIPE, VerticalAlignmentType.TOP);
          createText(lObj382, "thinHorzStripe");
          Cell lObj383 = createCell(lObj381);
          createCellDef(lObj383, null, null, color2, ShadingPattern.THIN_HORZ_STRIPE, VerticalAlignmentType.TOP);
          createText(lObj383, "thinHorzStripe");
        }
        Row lObj384 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj385 = createCell(lObj384);
          createCellDef(lObj385, null, null, color1, ShadingPattern.THIN_REVERSE_DIAG_STRIPE, VerticalAlignmentType.TOP);
          createText(lObj385, "thinReverseDiagStripe");
          Cell lObj386 = createCell(lObj384);
          createCellDef(lObj386, null, null, color2, ShadingPattern.THIN_REVERSE_DIAG_STRIPE, VerticalAlignmentType.TOP);
          createText(lObj386, "thinReverseDiagStripe");
        }
        Row lObj387 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj388 = createCell(lObj387);
          createCellDef(lObj388, null, null, color1, ShadingPattern.THIN_VERT_STRIPE, VerticalAlignmentType.TOP);
          createText(lObj388, "thinVertStripe");
          Cell lObj389 = createCell(lObj387);
          createCellDef(lObj389, null, null, color2, ShadingPattern.THIN_VERT_STRIPE, VerticalAlignmentType.TOP);
          createText(lObj389, "thinVertStripe");
        }
        Row lObj390 = createRow(lObj278, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj391 = createCell(lObj390);
          createCellDef(lObj391, null, null, color1, ShadingPattern.VERT_STRIPE, VerticalAlignmentType.TOP);
          createText(lObj391, "vertStripe");
          Cell lObj392 = createCell(lObj390);
          createCellDef(lObj392, null, null, color2, ShadingPattern.VERT_STRIPE, VerticalAlignmentType.TOP);
          createText(lObj392, "vertStripe");
        }
      }
      HeadingN lObj393 = createHeadingN(lObj0, 2, false);
      createText(lObj393, "Colors defined globally on the table");
      StyleApplication lObj394 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj394, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table shadingColor:\"225,225,225\" {\r\n    for (var i = 0; i < 3; i++) {\r\n        row {\r\n            cell {\"Some text\" }\r\n            cell {\"Some text\" }\r\n            cell {\"Some text\" }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj395 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj395, "Result");
      Table lObj396 = createTable(lObj0, null, null, null, "225,225,225", ShadingPattern.NO_VALUE);
      for (int i = 0; (i < 3); i++) {
        Row lObj397 = createRow(lObj396, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj398 = createCell(lObj397);
          createText(lObj398, "Some text");
          Cell lObj399 = createCell(lObj397);
          createText(lObj399, "Some text");
          Cell lObj400 = createCell(lObj397);
          createText(lObj400, "Some text");
        }
      }
      HeadingN lObj401 = createHeadingN(lObj0, 2, false);
      createText(lObj401, "Vertical Alignments");
      StyleApplication lObj402 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj402, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table {\r\n    row header {\r\n        cell {\"Default aligned\" }\r\n        cell {\"Bottom aligned\" }\r\n        cell {\"Center aligned\" }\r\n        cell {\"Top aligned\" }\r\n    }\r\n    row [ |vAlign:bottom | vAlign:center | vAlign:top ] {\r\n        cell {\"Some text Some textSome textSome textSome textSome textSome textSome textSome textSome textSome textSome textSome textSome text\" }\r\n        cell {\"Some text\" }\r\n        cell {\"Some text\" }\r\n        cell {\"Some text\" }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj403 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj403, "Result");
      Table lObj404 = createTable(lObj0, null, null, null, null, ShadingPattern.NO_VALUE);
      {
        Row lObj405 = createRow(lObj404, null, true, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj406 = createCell(lObj405);
          createText(lObj406, "Default aligned");
          Cell lObj407 = createCell(lObj405);
          createText(lObj407, "Bottom aligned");
          Cell lObj408 = createCell(lObj405);
          createText(lObj408, "Center aligned");
          Cell lObj409 = createCell(lObj405);
          createText(lObj409, "Top aligned");
        }
        Row lObj410 = createRow(lObj404, null, false, null, ShadingPattern.NO_VALUE, false, null);
        createCellDef(lObj410, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
        createCellDef(lObj410, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.BOTTOM);
        createCellDef(lObj410, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.CENTER);
        createCellDef(lObj410, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
        {
          Cell lObj411 = createCell(lObj410);
          createText(lObj411, "Some text Some textSome textSome textSome textSome textSome textSome textSome textSome textSome textSome textSome textSome text");
          Cell lObj412 = createCell(lObj410);
          createText(lObj412, "Some text");
          Cell lObj413 = createCell(lObj410);
          createText(lObj413, "Some text");
          Cell lObj414 = createCell(lObj410);
          createText(lObj414, "Some text");
        }
      }
      HeadingN lObj415 = createHeadingN(lObj0, 2, false);
      createText(lObj415, "No Borders");
      StyleApplication lObj416 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj416, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table [ borders(type:none) | borders(type:none)  | borders(type:none) ] {\r\n    for (var i = 0; i < 3; i++) {\r\n        row {\r\n            cell {\"Some text\" }\r\n            cell {\"Some text\" }\r\n            cell {\"Some text\" }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj417 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj417, "Result");
      Table lObj418 = createTable(lObj0, null, null, null, null, ShadingPattern.NO_VALUE);
      CellDef cellDef = createCellDef(lObj418, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      cellDef.setLeftBorder(createBorder(BorderType.NONE, null, -1));
      cellDef.setRightBorder(createBorder(BorderType.NONE, null, -1));
      cellDef.setTopBorder(createBorder(BorderType.NONE, null, -1));
      cellDef.setBottomBorder(createBorder(BorderType.NONE, null, -1));
      CellDef cellDef_1 = createCellDef(lObj418, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      cellDef_1.setLeftBorder(createBorder(BorderType.NONE, null, -1));
      cellDef_1.setRightBorder(createBorder(BorderType.NONE, null, -1));
      cellDef_1.setTopBorder(createBorder(BorderType.NONE, null, -1));
      cellDef_1.setBottomBorder(createBorder(BorderType.NONE, null, -1));
      CellDef cellDef_2 = createCellDef(lObj418, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      cellDef_2.setLeftBorder(createBorder(BorderType.NONE, null, -1));
      cellDef_2.setRightBorder(createBorder(BorderType.NONE, null, -1));
      cellDef_2.setTopBorder(createBorder(BorderType.NONE, null, -1));
      cellDef_2.setBottomBorder(createBorder(BorderType.NONE, null, -1));
      for (int i = 0; (i < 3); i++) {
        Row lObj419 = createRow(lObj418, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj420 = createCell(lObj419);
          createText(lObj420, "Some text");
          Cell lObj421 = createCell(lObj419);
          createText(lObj421, "Some text");
          Cell lObj422 = createCell(lObj419);
          createText(lObj422, "Some text");
        }
      }
      HeadingN lObj423 = createHeadingN(lObj0, 2, false);
      createText(lObj423, "No Borders odd row");
      StyleApplication lObj424 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj424, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table {\r\n    for (var i = 0; i < 7; i++) {\r\n        if ((i % 2) == 0) {\r\n            row [ borders(type:none) | borders(type:none)  | borders(type:none) ] {\r\n                cell {\"Some text without borders\" }\r\n                cell {\"Some text without borders\" }\r\n                cell {\"Some text without borders\" }\r\n            }\r\n        } else {\r\n            row {\r\n                cell {\"Some text\" }\r\n                cell {\"Some text\" }\r\n                cell {\"Some text\" }\r\n            }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj425 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj425, "Result");
      Table lObj426 = createTable(lObj0, null, null, null, null, ShadingPattern.NO_VALUE);
      for (int i = 0; (i < 7); i++) {
        if (((i % 2) == 0)) {
          Row lObj427 = createRow(lObj426, null, false, null, ShadingPattern.NO_VALUE, false, null);
          CellDef cellDef_3 = createCellDef(lObj427, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          cellDef_3.setLeftBorder(createBorder(BorderType.NONE, null, -1));
          cellDef_3.setRightBorder(createBorder(BorderType.NONE, null, -1));
          cellDef_3.setTopBorder(createBorder(BorderType.NONE, null, -1));
          cellDef_3.setBottomBorder(createBorder(BorderType.NONE, null, -1));
          CellDef cellDef_4 = createCellDef(lObj427, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          cellDef_4.setLeftBorder(createBorder(BorderType.NONE, null, -1));
          cellDef_4.setRightBorder(createBorder(BorderType.NONE, null, -1));
          cellDef_4.setTopBorder(createBorder(BorderType.NONE, null, -1));
          cellDef_4.setBottomBorder(createBorder(BorderType.NONE, null, -1));
          CellDef cellDef_5 = createCellDef(lObj427, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          cellDef_5.setLeftBorder(createBorder(BorderType.NONE, null, -1));
          cellDef_5.setRightBorder(createBorder(BorderType.NONE, null, -1));
          cellDef_5.setTopBorder(createBorder(BorderType.NONE, null, -1));
          cellDef_5.setBottomBorder(createBorder(BorderType.NONE, null, -1));
          {
            Cell lObj428 = createCell(lObj427);
            createText(lObj428, "Some text without borders");
            Cell lObj429 = createCell(lObj427);
            createText(lObj429, "Some text without borders");
            Cell lObj430 = createCell(lObj427);
            createText(lObj430, "Some text without borders");
          }
        } else {
          Row lObj431 = createRow(lObj426, null, false, null, ShadingPattern.NO_VALUE, false, null);
          {
            Cell lObj432 = createCell(lObj431);
            createText(lObj432, "Some text");
            Cell lObj433 = createCell(lObj431);
            createText(lObj433, "Some text");
            Cell lObj434 = createCell(lObj431);
            createText(lObj434, "Some text");
          }
        }
      }
      HeadingN lObj435 = createHeadingN(lObj0, 2, false);
      createText(lObj435, "No Borders per cell");
      StyleApplication lObj436 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj436, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table {\r\n    row {\r\n        cell {\"Some text\" }\r\n        cell [ bottomBorder(type:none)] {\"Some text\" }\r\n        cell {\"Some text\" }\r\n    }\r\n    row {\r\n        cell [ rightBorder(type:none)] {\"Some text\" }\r\n        cell [borders(type:none)] {\"No Borders\" }\r\n        cell [ leftBorder(type:none)] {\"Some text\" }\r\n    }\r\n    row {\r\n        cell {\"Some text\" }\r\n        cell [ topBorder(type:none)] {\"Some text\" }\r\n        cell {\"Some text\" }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj437 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj437, "Result");
      Table lObj438 = createTable(lObj0, null, null, null, null, ShadingPattern.NO_VALUE);
      {
        Row lObj439 = createRow(lObj438, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj440 = createCell(lObj439);
          createText(lObj440, "Some text");
          Cell lObj441 = createCell(lObj439);
          CellDef cellDef_3 = createCellDef(lObj441, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          cellDef_3.setBottomBorder(createBorder(BorderType.NONE, null, -1));
          createText(lObj441, "Some text");
          Cell lObj442 = createCell(lObj439);
          createText(lObj442, "Some text");
        }
        Row lObj443 = createRow(lObj438, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj444 = createCell(lObj443);
          CellDef cellDef_3 = createCellDef(lObj444, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          cellDef_3.setRightBorder(createBorder(BorderType.NONE, null, -1));
          createText(lObj444, "Some text");
          Cell lObj445 = createCell(lObj443);
          CellDef cellDef_4 = createCellDef(lObj445, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          cellDef_4.setLeftBorder(createBorder(BorderType.NONE, null, -1));
          cellDef_4.setRightBorder(createBorder(BorderType.NONE, null, -1));
          cellDef_4.setTopBorder(createBorder(BorderType.NONE, null, -1));
          cellDef_4.setBottomBorder(createBorder(BorderType.NONE, null, -1));
          createText(lObj445, "No Borders");
          Cell lObj446 = createCell(lObj443);
          CellDef cellDef_5 = createCellDef(lObj446, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          cellDef_5.setLeftBorder(createBorder(BorderType.NONE, null, -1));
          createText(lObj446, "Some text");
        }
        Row lObj447 = createRow(lObj438, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj448 = createCell(lObj447);
          createText(lObj448, "Some text");
          Cell lObj449 = createCell(lObj447);
          CellDef cellDef_3 = createCellDef(lObj449, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
          cellDef_3.setTopBorder(createBorder(BorderType.NONE, null, -1));
          createText(lObj449, "Some text");
          Cell lObj450 = createCell(lObj447);
          createText(lObj450, "Some text");
        }
      }
      HeadingN lObj451 = createHeadingN(lObj0, 2, false);
      createText(lObj451, "Borders");
      HeadingN lObj452 = createHeadingN(lObj0, (2 + 1), false);
      createText(lObj452, "Borders defined globally on table level");
      StyleApplication lObj453 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj453, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table borders(type:DOUBLE size:16 color:\"0,128,192\") {\r\n    for (var i = 0; i < 3; i++) {\r\n        row {\r\n            cell {\"Some text\" }\r\n            cell {\"Some text\" }\r\n            cell {\"Some text\" }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj454 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj454, "Result");
      Table lObj455 = createTable(lObj0, null, null, createBorder(BorderType.DOUBLE, "0,128,192", 16), null, ShadingPattern.NO_VALUE);
      for (int i = 0; (i < 3); i++) {
        Row lObj456 = createRow(lObj455, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj457 = createCell(lObj456);
          createText(lObj457, "Some text");
          Cell lObj458 = createCell(lObj456);
          createText(lObj458, "Some text");
          Cell lObj459 = createCell(lObj456);
          createText(lObj459, "Some text");
        }
      }
      HeadingN lObj460 = createHeadingN(lObj0, (2 + 1), false);
      createText(lObj460, "No-Borders defined globally on table level");
      StyleApplication lObj461 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj461, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table borders(type:none size:16 color:\"0,128,192\") {\r\n    for (var i = 0; i < 3; i++) {\r\n        row {\r\n            cell {\"Some text\" }\r\n            cell {\"Some text\" } \r\n            cell {\"Some text\" }\r\n        }\r\n    }\r\n}", null), lObj0);
      
      StyleApplication lObj462 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj462, "Result");
      Table lObj463 = createTable(lObj0, null, null, createBorder(BorderType.NONE, "0,128,192", 16), null, ShadingPattern.NO_VALUE);
      for (int i = 0; (i < 3); i++) {
        Row lObj464 = createRow(lObj463, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj465 = createCell(lObj464);
          createText(lObj465, "Some text");
          Cell lObj466 = createCell(lObj464);
          createText(lObj466, "Some text");
          Cell lObj467 = createCell(lObj464);
          createText(lObj467, "Some text");
        }
      }
      HeadingN lObj468 = createHeadingN(lObj0, (2 + 1), false);
      createText(lObj468, "Borders defined for each column on table level");
      StyleApplication lObj469 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj469, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table [ borders(size:8 type:doubleWave color:\"0,128,128\")|borders(size:8 type:doubleWave color:\"0,128,128\")|borders(size:8 type:doubleWave color:\"0,128,128\")] {\r\n\t\t\tfor (var i = 0; i < 3; i++) {\r\n\t\t\t\trow {\r\n\t\t\t\t\tcell {\"Some text\" }\r\n\t\t\t\t\tcell {\"Some text\" }\r\n\t\t\t\t\tcell {\"Some text\" }\r\n\t\t\t\t}\r\n\t\t\t}\r\n\t\t}", null), lObj0);
      
      StyleApplication lObj470 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj470, "Result");
      Table lObj471 = createTable(lObj0, null, null, null, null, ShadingPattern.NO_VALUE);
      CellDef cellDef_3 = createCellDef(lObj471, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      cellDef_3.setLeftBorder(createBorder(BorderType.DOUBLE_WAVE, "0,128,128", 8));
      cellDef_3.setRightBorder(createBorder(BorderType.DOUBLE_WAVE, "0,128,128", 8));
      cellDef_3.setTopBorder(createBorder(BorderType.DOUBLE_WAVE, "0,128,128", 8));
      cellDef_3.setBottomBorder(createBorder(BorderType.DOUBLE_WAVE, "0,128,128", 8));
      CellDef cellDef_4 = createCellDef(lObj471, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      cellDef_4.setLeftBorder(createBorder(BorderType.DOUBLE_WAVE, "0,128,128", 8));
      cellDef_4.setRightBorder(createBorder(BorderType.DOUBLE_WAVE, "0,128,128", 8));
      cellDef_4.setTopBorder(createBorder(BorderType.DOUBLE_WAVE, "0,128,128", 8));
      cellDef_4.setBottomBorder(createBorder(BorderType.DOUBLE_WAVE, "0,128,128", 8));
      CellDef cellDef_5 = createCellDef(lObj471, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      cellDef_5.setLeftBorder(createBorder(BorderType.DOUBLE_WAVE, "0,128,128", 8));
      cellDef_5.setRightBorder(createBorder(BorderType.DOUBLE_WAVE, "0,128,128", 8));
      cellDef_5.setTopBorder(createBorder(BorderType.DOUBLE_WAVE, "0,128,128", 8));
      cellDef_5.setBottomBorder(createBorder(BorderType.DOUBLE_WAVE, "0,128,128", 8));
      for (int i = 0; (i < 3); i++) {
        Row lObj472 = createRow(lObj471, null, false, null, ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj473 = createCell(lObj472);
          createText(lObj473, "Some text");
          Cell lObj474 = createCell(lObj472);
          createText(lObj474, "Some text");
          Cell lObj475 = createCell(lObj472);
          createText(lObj475, "Some text");
        }
      }
      HeadingN lObj476 = createHeadingN(lObj0, (2 + 1), false);
      createText(lObj476, "Borders defined on row level");
      StyleApplication lObj477 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj477, "Source");
      addAllToParent(getLanguageRenderer("pxdoc").render(lObj0, "table {\r\n\trow [ borders(size:8 type:dashed color:\"0,128,128\")|borders(size:16 type:doubleWave color:\"0,128,0\")|borders(size:32 type:thickThinLargeGap color:\"128,128,128\") ] {\r\n\t\tcell {\"Some text\" }\r\n\t\tcell {\"Some text\" }\r\n\t\tcell {\"Some text\" }\r\n\t}\r\n\trow [ borders(size:8 type:single color:\"0,128,128\")|borders(size:16 type:dotted color:\"0,128,0\")|borders(size:32 type:DOUBLE color:\"128,128,128\") ] {\r\n\t\tcell {\"Some text\" }\r\n\t\tcell {\"Some text\" }\r\n\t\tcell {\"Some text\" }\r\n\t}\r\n\trow [ borders(size:8 type:dashSmallGap color:\"0,0,255\")|borders(size:16 type:outset color:\"0,128,0\")|borders(size:32 type:thinThickThinLargeGap color:\"64,128,128\") ] {\r\n\t\tcell {\"Some text\" }\r\n\t\tcell {\"Some text\" }\r\n\t\tcell {\"Some text\" }\r\n\t}\r\n}", null), lObj0);
      
      StyleApplication lObj478 = createStyleApplication(lObj0, false, "UsageOfpxDoc", null, null, null);
      createText(lObj478, "Result");
      Table lObj479 = createTable(lObj0, null, null, null, null, ShadingPattern.NO_VALUE);
      {
        Row lObj480 = createRow(lObj479, null, false, null, ShadingPattern.NO_VALUE, false, null);
        CellDef cellDef_6 = createCellDef(lObj480, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
        cellDef_6.setLeftBorder(createBorder(BorderType.DASHED, "0,128,128", 8));
        cellDef_6.setRightBorder(createBorder(BorderType.DASHED, "0,128,128", 8));
        cellDef_6.setTopBorder(createBorder(BorderType.DASHED, "0,128,128", 8));
        cellDef_6.setBottomBorder(createBorder(BorderType.DASHED, "0,128,128", 8));
        CellDef cellDef_7 = createCellDef(lObj480, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
        cellDef_7.setLeftBorder(createBorder(BorderType.DOUBLE_WAVE, "0,128,0", 16));
        cellDef_7.setRightBorder(createBorder(BorderType.DOUBLE_WAVE, "0,128,0", 16));
        cellDef_7.setTopBorder(createBorder(BorderType.DOUBLE_WAVE, "0,128,0", 16));
        cellDef_7.setBottomBorder(createBorder(BorderType.DOUBLE_WAVE, "0,128,0", 16));
        CellDef cellDef_8 = createCellDef(lObj480, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
        cellDef_8.setLeftBorder(createBorder(BorderType.THICK_THIN_LARGE_GAP, "128,128,128", 32));
        cellDef_8.setRightBorder(createBorder(BorderType.THICK_THIN_LARGE_GAP, "128,128,128", 32));
        cellDef_8.setTopBorder(createBorder(BorderType.THICK_THIN_LARGE_GAP, "128,128,128", 32));
        cellDef_8.setBottomBorder(createBorder(BorderType.THICK_THIN_LARGE_GAP, "128,128,128", 32));
        {
          Cell lObj481 = createCell(lObj480);
          createText(lObj481, "Some text");
          Cell lObj482 = createCell(lObj480);
          createText(lObj482, "Some text");
          Cell lObj483 = createCell(lObj480);
          createText(lObj483, "Some text");
        }
        Row lObj484 = createRow(lObj479, null, false, null, ShadingPattern.NO_VALUE, false, null);
        CellDef cellDef_9 = createCellDef(lObj484, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
        cellDef_9.setLeftBorder(createBorder(BorderType.SINGLE, "0,128,128", 8));
        cellDef_9.setRightBorder(createBorder(BorderType.SINGLE, "0,128,128", 8));
        cellDef_9.setTopBorder(createBorder(BorderType.SINGLE, "0,128,128", 8));
        cellDef_9.setBottomBorder(createBorder(BorderType.SINGLE, "0,128,128", 8));
        CellDef cellDef_10 = createCellDef(lObj484, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
        cellDef_10.setLeftBorder(createBorder(BorderType.DOTTED, "0,128,0", 16));
        cellDef_10.setRightBorder(createBorder(BorderType.DOTTED, "0,128,0", 16));
        cellDef_10.setTopBorder(createBorder(BorderType.DOTTED, "0,128,0", 16));
        cellDef_10.setBottomBorder(createBorder(BorderType.DOTTED, "0,128,0", 16));
        CellDef cellDef_11 = createCellDef(lObj484, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
        cellDef_11.setLeftBorder(createBorder(BorderType.DOUBLE, "128,128,128", 32));
        cellDef_11.setRightBorder(createBorder(BorderType.DOUBLE, "128,128,128", 32));
        cellDef_11.setTopBorder(createBorder(BorderType.DOUBLE, "128,128,128", 32));
        cellDef_11.setBottomBorder(createBorder(BorderType.DOUBLE, "128,128,128", 32));
        {
          Cell lObj485 = createCell(lObj484);
          createText(lObj485, "Some text");
          Cell lObj486 = createCell(lObj484);
          createText(lObj486, "Some text");
          Cell lObj487 = createCell(lObj484);
          createText(lObj487, "Some text");
        }
        Row lObj488 = createRow(lObj479, null, false, null, ShadingPattern.NO_VALUE, false, null);
        CellDef cellDef_12 = createCellDef(lObj488, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
        cellDef_12.setLeftBorder(createBorder(BorderType.DASH_SMALL_GAP, "0,0,255", 8));
        cellDef_12.setRightBorder(createBorder(BorderType.DASH_SMALL_GAP, "0,0,255", 8));
        cellDef_12.setTopBorder(createBorder(BorderType.DASH_SMALL_GAP, "0,0,255", 8));
        cellDef_12.setBottomBorder(createBorder(BorderType.DASH_SMALL_GAP, "0,0,255", 8));
        CellDef cellDef_13 = createCellDef(lObj488, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
        cellDef_13.setLeftBorder(createBorder(BorderType.OUTSET, "0,128,0", 16));
        cellDef_13.setRightBorder(createBorder(BorderType.OUTSET, "0,128,0", 16));
        cellDef_13.setTopBorder(createBorder(BorderType.OUTSET, "0,128,0", 16));
        cellDef_13.setBottomBorder(createBorder(BorderType.OUTSET, "0,128,0", 16));
        CellDef cellDef_14 = createCellDef(lObj488, null, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
        cellDef_14.setLeftBorder(createBorder(BorderType.THIN_THICK_THIN_LARGE_GAP, "64,128,128", 32));
        cellDef_14.setRightBorder(createBorder(BorderType.THIN_THICK_THIN_LARGE_GAP, "64,128,128", 32));
        cellDef_14.setTopBorder(createBorder(BorderType.THIN_THICK_THIN_LARGE_GAP, "64,128,128", 32));
        cellDef_14.setBottomBorder(createBorder(BorderType.THIN_THICK_THIN_LARGE_GAP, "64,128,128", 32));
        {
          Cell lObj489 = createCell(lObj488);
          createText(lObj489, "Some text");
          Cell lObj490 = createCell(lObj488);
          createText(lObj490, "Some text");
          Cell lObj491 = createCell(lObj488);
          createText(lObj491, "Some text");
        }
      }
    }
  }
}
