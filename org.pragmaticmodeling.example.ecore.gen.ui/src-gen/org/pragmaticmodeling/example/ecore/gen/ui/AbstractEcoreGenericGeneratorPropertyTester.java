package org.pragmaticmodeling.example.ecore.gen.ui;

import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.commands.DefaultPropertyTester;
import org.eclipse.emf.ecore.EObject;

/**
 * PxDocModel property tester and model provider
 */
public class AbstractEcoreGenericGeneratorPropertyTester extends DefaultPropertyTester {

	public AbstractEcoreGenericGeneratorPropertyTester() {
		super(EObject.class);
	}
}

