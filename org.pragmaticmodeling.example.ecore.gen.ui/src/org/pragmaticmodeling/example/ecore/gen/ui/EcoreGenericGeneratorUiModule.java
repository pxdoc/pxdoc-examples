package org.pragmaticmodeling.example.ecore.gen.ui;

import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.AbstractPxUiPlugin;

public class EcoreGenericGeneratorUiModule extends AbstractEcoreGenericGeneratorUiModule {
		   
	public EcoreGenericGeneratorUiModule(AbstractPxUiPlugin plugin) {
		super(plugin);
	}
	
}
	
