package org.pragmaticmodeling.example.mygenerator;
				
import com.google.inject.Binder;
import com.google.inject.Module;
				
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
					
public abstract class AbstractLibraryGeneratorModule implements Module {
					
	@Override
	public void configure(Binder binder) {
		binder.bind(IPxDocGenerator.class).to(LibraryGenerator.class);
	}

}