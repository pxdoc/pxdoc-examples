package org.pragmaticmodeling.pxdoc.emf.gen.ui;

/**
 * Wizard class
 */
public class EMFExampleWizard extends AbstractEMFExampleWizard {

	/**
	 * Add the custom page to the generation launching wizard
	 */
	
	@Override
	public void addPages() {
		super.addPages();
		addPage(new EMFSampleOptionsPage("Custom Options"));
	}

}
	 
	
