package org.pragmaticmodeling.pxdoc.emf.gen.ui;

import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.commands.DefaultPropertyTester;
import org.eclipse.emf.ecore.EObject;

/**
 * PxDocModel property tester and model provider
 */
public class AbstractEMFExamplePropertyTester extends DefaultPropertyTester {

	public AbstractEMFExamplePropertyTester() {
		super(EObject.class);
	}
}

