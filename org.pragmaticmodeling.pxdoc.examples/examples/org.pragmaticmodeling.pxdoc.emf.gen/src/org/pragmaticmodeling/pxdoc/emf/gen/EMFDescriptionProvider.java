package org.pragmaticmodeling.pxdoc.emf.gen;

import org.pragmaticmodeling.pxdoc.common.lib.DefaultDescriptionProvider;

import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;

public class EMFDescriptionProvider extends DefaultDescriptionProvider {

	public EMFDescriptionProvider(IPxDocGenerator generator) {
		super(generator);
	}	

}
