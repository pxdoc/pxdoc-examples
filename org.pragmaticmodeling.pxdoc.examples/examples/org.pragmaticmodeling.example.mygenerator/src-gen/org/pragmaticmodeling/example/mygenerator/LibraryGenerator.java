package org.pragmaticmodeling.example.mygenerator;

import fr.pragmaticmodeling.pxdoc.AlignmentType;
import fr.pragmaticmodeling.pxdoc.Bold;
import fr.pragmaticmodeling.pxdoc.Cell;
import fr.pragmaticmodeling.pxdoc.ContainerElement;
import fr.pragmaticmodeling.pxdoc.Document;
import fr.pragmaticmodeling.pxdoc.Font;
import fr.pragmaticmodeling.pxdoc.HorizontalAlignment;
import fr.pragmaticmodeling.pxdoc.HorizontalRelative;
import fr.pragmaticmodeling.pxdoc.Italic;
import fr.pragmaticmodeling.pxdoc.Measure;
import fr.pragmaticmodeling.pxdoc.MergeKind;
import fr.pragmaticmodeling.pxdoc.Row;
import fr.pragmaticmodeling.pxdoc.Section;
import fr.pragmaticmodeling.pxdoc.SectionKind;
import fr.pragmaticmodeling.pxdoc.ShadingPattern;
import fr.pragmaticmodeling.pxdoc.StyleApplication;
import fr.pragmaticmodeling.pxdoc.Table;
import fr.pragmaticmodeling.pxdoc.UnderlineType;
import fr.pragmaticmodeling.pxdoc.UnitKind;
import fr.pragmaticmodeling.pxdoc.VerticalAlignment;
import fr.pragmaticmodeling.pxdoc.VerticalAlignmentType;
import fr.pragmaticmodeling.pxdoc.VerticalRelative;
import fr.pragmaticmodeling.pxdoc.runtime.AbstractPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.PxDocGenerationException;
import java.io.File;
import java.util.List;
import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.pragmaticmodeling.example.library.Author;
import org.pragmaticmodeling.example.library.Book;
import org.pragmaticmodeling.example.library.Category;
import org.pragmaticmodeling.example.library.Library;

@SuppressWarnings("all")
public class LibraryGenerator extends AbstractPxDocGenerator {
  private Logger logger = Logger.getLogger(getClass());
  
  public LibraryGenerator() {
    super();
  }
  
  public void generate() throws PxDocGenerationException {
    org.pragmaticmodeling.example.library.Library library = (org.pragmaticmodeling.example.library.Library)launcher.getModel().getInstance();
    try {
    	logger.info("----------------------------------------------------------------");
    	logger.info("Starting LibraryGenerator generation...");
    	checkStylesheet(getStylesheetName());
    	main(null, library);
    } catch (Exception e) {
    	throw new PxDocGenerationException(e);
    } catch (NoClassDefFoundError e) {
        logger.error(e.getMessage());
    } finally {
    	logger.info("FINISHED: LibraryGenerator generation...");
    }
  }
  
  @Override
  public String getStylesheetName() {
    return "MyStylesheet";
  }
  
  public void error(final String message) {
    logger.error(message);
  }
  
  public void info(final String message) {
    logger.info(message);
  }
  
  public void main(final ContainerElement parent, final Library library) throws PxDocGenerationException {
    String _file = (new File(getLauncher().getTargetFile())).getAbsolutePath();
    Document lObj0 = createDocument(parent, _file, "MyStylesheet", null, createMeasure(getHeaderHeight(), UnitKind.CM), createMeasure(getFooterHeight(), UnitKind.CM));
    addDocument(lObj0);
    EList<Author> _authors = library.getAuthors();
    for (final Author author : _authors) {
      {
        StyleApplication lObj1 = createStyleApplication(lObj0, false, "Heading1", null, null, null);
        {
          createText(lObj1, author.getName());
          Measure imageHeight2 = createMeasure((float)2.8, UnitKind.CM);
          createImage(lObj1, author.getPicture(), null, null, imageHeight2, createHorizontalPosition(HorizontalAlignment.RIGHT, HorizontalRelative.MARGIN), createVerticalPosition(VerticalAlignment.CENTER, VerticalRelative.LINE));
        }
        createLineBreak(lObj0);
        Font lObj3 = createFont(lObj0, null, null, true, false, true, UnderlineType.SINGLE, false, null, "122,91,255", null);
        createText(lObj3, "Biography");
        createLineBreak(lObj0);
        Section section4 = createSection(lObj0, SectionKind.CONTINUOUS, false, 2, null);
        Italic lObj5 = createItalic(lObj0);
        createText(lObj5, author.getBiography());
        Section section6 = createSection(lObj0, SectionKind.CONTINUOUS, false, 1, null);
        createParagraphBreak(lObj0);
        Measure tableLeft7 = createMeasure((float)2.5, UnitKind.CM);
        Table lObj8 = createTable(lObj0, tableLeft7, null, createBorder(null, "84,138,183", -1), null, ShadingPattern.NO_VALUE);
        Measure cellWidth9 = createMeasure((float)20, UnitKind.PC);
        createCellDef(lObj8, null, cellWidth9, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.CENTER);
        Measure cellWidth10 = createMeasure((float)40, UnitKind.PC);
        createCellDef(lObj8, null, cellWidth10, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.CENTER);
        Measure cellWidth11 = createMeasure((float)20, UnitKind.PC);
        createCellDef(lObj8, null, cellWidth11, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.CENTER);
        {
          Row lObj12 = createRow(lObj8, null, true, "84,138,183", ShadingPattern.NO_VALUE, false, null);
          {
            Cell lObj13 = createCell(lObj12);
            StyleApplication lObj14 = createStyleApplication(lObj13, false, "CellTitle", AlignmentType.CENTER, null, null);
            createText(lObj14, "Category");
            Cell lObj15 = createCell(lObj12);
            StyleApplication lObj16 = createStyleApplication(lObj15, false, "CellTitle", null, null, null);
            createText(lObj16, "Title");
            Cell lObj17 = createCell(lObj12);
            StyleApplication lObj18 = createStyleApplication(lObj17, false, "CellTitle", AlignmentType.CENTER, null, null);
            createText(lObj18, "Publication date");
          }
          final Function1<Book, Category> _function = (Book it) -> {
            return it.getCategory();
          };
          List<Book> _sortBy = IterableExtensions.<Book, Category>sortBy(author.getBooks(), _function);
          for (final Book book : _sortBy) {
            Row lObj19 = createRow(lObj8, null, false, null, ShadingPattern.NO_VALUE, false, null);
            {
              Cell lObj20 = createCell(lObj19);
              createCellDef(lObj20, MergeKind.AUTOMATIC, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
              StyleApplication lObj21 = createStyleApplication(lObj20, false, "Normal", AlignmentType.CENTER, null, null);
              createText(lObj21, book.getCategory().getName());
              Cell lObj22 = createCell(lObj19);
              Bold lObj23 = createBold(lObj22);
              createText(lObj23, book.getTitle());
              Cell lObj24 = createCell(lObj19);
              StyleApplication lObj25 = createStyleApplication(lObj24, false, "Normal", AlignmentType.CENTER, null, null);
              Italic lObj26 = createItalic(lObj25);
              createText(lObj26, String.valueOf(book.getPublicationDate()));
            }
          }
        }
        createParagraphBreak(lObj0);
      }
    }
  }
}
