package org.pragmaticmodeling.example.mygenerator;

import fr.pragmaticmodeling.pxdoc.ContainerElement;
import fr.pragmaticmodeling.pxdoc.Document;
import fr.pragmaticmodeling.pxdoc.UnitKind;
import fr.pragmaticmodeling.pxdoc.runtime.AbstractPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.PxDocGenerationException;
import java.io.File;
import org.apache.log4j.Logger;
import org.pragmaticmodeling.example.library.Library;

@SuppressWarnings("all")
public class LibraryGenerator extends AbstractPxDocGenerator {
  private Logger logger = Logger.getLogger(getClass());
  
  public LibraryGenerator() {
    super();
  }
  
  public void generate() throws PxDocGenerationException {
    org.pragmaticmodeling.example.library.Library library = (org.pragmaticmodeling.example.library.Library)launcher.getModel().getInstance();
    try {
    	logger.info("----------------------------------------------------------------");
    	logger.info("Starting LibraryGenerator generation...");
    	checkStylesheet(getStylesheetName());
    	main(null, library);
    } catch (Exception e) {
    	throw new PxDocGenerationException(e);
    } catch (NoClassDefFoundError e) {
        logger.error(e.getMessage());
    } finally {
    	logger.info("FINISHED: LibraryGenerator generation...");
    }
  }
  
  @Override
  public String getStylesheetName() {
    return "MyStylesheet";
  }
  
  public void error(final String message) {
    logger.error(message);
  }
  
  public void info(final String message) {
    logger.info(message);
  }
  
  public void main(final ContainerElement parent, final Library library) throws PxDocGenerationException {
    String _file = (new File(getLauncher().getTargetFile())).getAbsolutePath();
    Document lObj0 = createDocument(parent, _file, "MyStylesheet", null, createMeasure(getHeaderHeight(), UnitKind.CM), createMeasure(getFooterHeight(), UnitKind.CM));
    addDocument(lObj0);
  }
}
