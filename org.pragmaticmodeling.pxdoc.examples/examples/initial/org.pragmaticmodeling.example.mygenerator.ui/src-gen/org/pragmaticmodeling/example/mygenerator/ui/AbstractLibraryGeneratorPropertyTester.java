package org.pragmaticmodeling.example.mygenerator.ui;

import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.commands.DefaultPropertyTester;

/**
 * PxDocModel property tester and model provider
 */
public class AbstractLibraryGeneratorPropertyTester extends DefaultPropertyTester {

	public AbstractLibraryGeneratorPropertyTester() {
		super(org.eclipse.emf.ecore.EObject.class);
	}
}

