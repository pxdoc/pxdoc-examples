package org.pragmaticmodeling.pxdoc.emf.gen.ui;

import org.osgi.framework.Bundle;

import com.google.inject.Injector;

import fr.pragmaticmodeling.pxdoc.runtime.eclipse.guice.AbstractGuiceAwareExecutableExtensionFactory;

public class EMFExampleExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	   protected Bundle getBundle() {
	       return EMFExampleActivator.getInstance().getBundle();
	   }
	    
	   @Override
	   protected Injector getInjector() {
	       return EMFExampleActivator.getInstance().getInjector();
	   }
	   
}
	
