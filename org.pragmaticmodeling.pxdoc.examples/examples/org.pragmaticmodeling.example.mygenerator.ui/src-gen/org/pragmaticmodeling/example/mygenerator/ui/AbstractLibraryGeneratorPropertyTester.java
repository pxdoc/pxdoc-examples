package org.pragmaticmodeling.example.mygenerator.ui;

import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.commands.DefaultPropertyTester;
import org.pragmaticmodeling.example.library.Library;

/**
 * PxDocModel property tester and model provider
 */
public class AbstractLibraryGeneratorPropertyTester extends DefaultPropertyTester {

	public AbstractLibraryGeneratorPropertyTester() {
		super(Library.class);
	}
}

