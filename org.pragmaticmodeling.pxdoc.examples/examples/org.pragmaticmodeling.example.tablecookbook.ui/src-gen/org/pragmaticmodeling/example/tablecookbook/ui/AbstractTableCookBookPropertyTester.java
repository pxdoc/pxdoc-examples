package org.pragmaticmodeling.example.tablecookbook.ui;

import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.commands.DefaultPropertyTester;
import org.eclipse.emf.ecore.EPackage;

/**
 * PxDocModel property tester and model provider
 */
public class AbstractTableCookBookPropertyTester extends DefaultPropertyTester {

	public AbstractTableCookBookPropertyTester() {
		super(EPackage.class);
	}
}

