package org.pragmaticmodeling.example.tablecookbook.ui;

import org.osgi.framework.Bundle;

import com.google.inject.Injector;

import fr.pragmaticmodeling.pxdoc.runtime.eclipse.guice.AbstractGuiceAwareExecutableExtensionFactory;

public class TableCookBookExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	   protected Bundle getBundle() {
	       return TableCookBookActivator.getInstance().getBundle();
	   }
	    
	   @Override
	   protected Injector getInjector() {
	       return TableCookBookActivator.getInstance().getInjector();
	   }
	   
}
	
