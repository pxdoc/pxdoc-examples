package org.pragmaticmodeling.example.tablecookbook.ui;
		
import org.apache.log4j.Logger;
import org.osgi.framework.BundleContext;

import com.google.inject.Guice;

import fr.pragmaticmodeling.common.guice.Modules2;
import org.pragmaticmodeling.example.tablecookbook.TableCookBookModule;
import fr.pragmaticmodeling.pxdoc.runtime.eclipse.AbstractPxGuiceAwarePlugin;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.AbstractPxGuiceAwareUiPlugin;
		
/**
* The activator class controls the plug-in life cycle
*/
public class TableCookBookActivator extends AbstractPxGuiceAwareUiPlugin {
	
	// The plug-in ID
	public static final String PLUGIN_ID = "org.pragmaticmodeling.example.tablecookbook.ui";
	public static final String GENERATOR_PLUGIN_ID = "org.pragmaticmodeling.example.tablecookbook";
	
	private static Logger logger = Logger.getLogger(AbstractPxGuiceAwarePlugin.class);
	
	private static TableCookBookActivator instance;
	
			
	/**
	 * The constructor
	 */
	public TableCookBookActivator() {
		super();
		instance = this;
	}
	
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		try {
			setInjector(Guice.createInjector(Modules2.mixin(new TableCookBookUiModule(this), new TableCookBookModule())));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		super.stop(context);
	}
	
	protected String getPluginId() {
		return PLUGIN_ID;	
	}
	
	public String getPxDocGeneratorPluginId() {
		return GENERATOR_PLUGIN_ID;	
	}
	
	public static TableCookBookActivator getInstance() {
		return instance;
	}
		
}