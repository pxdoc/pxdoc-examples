package org.pragmaticmodeling.example.ecore.gen

import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EDataType
import org.eclipse.emf.ecore.EEnum
import org.eclipse.emf.ecore.EModelElement
import org.eclipse.emf.ecore.ENamedElement
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EOperation
import org.eclipse.emf.ecore.EPackage
import org.pragmaticmodeling.pxdoc.common.lib.CommonServices
import org.pragmaticmodeling.pxdoc.common.lib.DefaultDescriptionProvider
import org.pragmaticmodeling.pxdoc.emf.lib.EmfServices

pxDocGenerator EcoreGenericGenerator stylesheet:eCoreStylesheet with: CommonServices, EmfServices styleBindings: Emphasis->Quote, BulletList->ListParagraph, BodyText->Normal, Figure->Caption
{


		root template main(EPackage model) {

		// Commons lib configuration : set a description provider able to get information about object description
		descriptionProvider = new DefaultDescriptionProvider(this)

		document {

			apply firstPageAndToc(model)
			apply ePkgDescription(model, 1)

		}

	}


	template ePkgDescription(EPackage model, int level) {
		apply commonDescription(model, level)

		HeadingN level:level + 1 {'Classes' }
		apply classDescription(model.eContents.filter(EClass), level + 2)
		HeadingN level:level + 1 {'Data Types' }
		apply dataTypesDescription(model.eContents.filter(EDataType), level + 2)
		HeadingN level:level + 1 {'Enums' }
		apply enumDescription(model.eContents.filter(EEnum), level + 2)

		// Apply recursively this template to the nested packages
		apply ePkgDescription(model.eContents.filter(EPackage), level)

	}


	template dataTypesDescription(EDataType dt, int level) {
		apply commonDescription(dt, level)

	}

	template enumDescription(EEnum enum, int level) {
		apply commonDescription(enum, level)
		#Subtitle {'Literals' }
		for (l : enum.ELiterals) {
			#ListParagraph { apply iconAndText(l) }
		}
	}

	template classDescription(EClass c, int level) {
		apply commonDescription(c, level)
//FIXME: passer la liste des argument (EAllAttributes,...) dans le template eTypedDescription, 
//ou modifier le template pour que les listes soient chopées la-bas 
		if (!c.EAllAttributes.empty) {
			apply eTypedDescription(c,  "Attributes")
		}
		if (!c.EAllReferences.empty) {
			apply eTypedDescription(c,  "References")
		}
		if (!c.EAllOperations.empty) {
			apply eTypedDescription(c,  "Operations")
		}
		§
		§
	}


	template eTypedDescription(EClass c, String text) {
		var EList list
		switch text {
			case "Attributes": {
				 list=c.EAllAttributes
			}
			default:{
				
			}
		}
		
		#Subtitle {text }
		table [ width:40pc | width:30pc | width:10pc | width:10pc ] {
			row header shadingColor:"155,177,200"{
				cell {'Name' }
				cell {'Type' }
				cell {'lower bound' } // FIXEME voir pour method getMultiplicity (1, 0..1,....)
				cell {'Upper bound' }
				if (text == 'Operations') {
					cell {'Parameters' }

				}
			}
			for (a : list.sortBy[name]) {
				row {
					cell {bold {a.name } }
					cell {a.getEType.name }
					cell {a.lowerBound }
					cell {a.upperBound }
					if (text === 'Operations') {
						cell { apply iconAndText((a as EOperation).EParameters) separator {§ } }
					}
				}
			}
		}
		§
		§
	}

	template commonDescription(EModelElement e, int level) {
		apply basicDescription(e, level)
		if (!e.getEAnnotations.empty) {
			#Subtitle {'Annotations' }
			§
			table {
				row {
					cell {bold {'Source' } }
					cell {bold {'References' } }
				}
				for (a : e.getEAnnotations) {
					row {
						cell {a.source }
						cell { apply iconAndText(a.references) separator {§ } }
					}
				}
			}
		}
	}

	template hyperlinkAndiconAndText(ENamedElement element) {
		hyperlink bookmark:element.validBookmark { apply iconAndText(element) }
	}

	template firstPageAndToc(EObject model) {
		§
		§
		§
		§
		§
		#Title align:center {'Description of the Ecore model' § apply iconAndText(model) }
		newPage
		toc
		§
	}

//	template basicDescription(EObject element, int level) {
//		HeadingN level:level keepWithNext {
//			if (element.hasBookmark) {
//				bookmark element.validBookmark { apply iconAndText(element) }
//			} else {
//				apply iconAndText(element)
//			}
//		}
//		apply optionalDocumentation(element)
//	}
}
