package org.pragmaticmodeling.example.ecore.gen;
				
import com.google.inject.Binder;
import com.google.inject.Module;
				
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
					
public abstract class AbstractEcoreGenericGeneratorModule implements Module {
					
	@Override
	public void configure(Binder binder) {
		binder.bind(IPxDocGenerator.class).to(EcoreGenericGenerator.class);
	}

}